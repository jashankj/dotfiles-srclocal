/**
 * @brief userland command to set intel powerclamp
 * @author Jashank Jeremy <jashank@rulingia.com>
 * @date 2016-12-31
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <sysexits.h>
#include <unistd.h>

#ifndef __VCSID
# define __IDSTRING(name, string) __asm__(".ident\t\"" string "\"")
# define __VCSID(s) __IDSTRING (__CONCAT (__rcsid_, __LINE__), s)
#endif

__VCSID("$Id: powerclamp.c,v 1.6 2016/12/31 01:54:07 jashank Exp $");

#define POWERCLAMP_PATH "/sys/class/thermal/cooling_device4/cur_state"
#define USAGE_MSG "usage: powerclamp <0..50>"

int
main (int argc, char *argv[]) {
	if (geteuid () != 0)
		errx (EX_USAGE, "should be run as root...");

	if (argc != 2)
		errx (EX_USAGE, USAGE_MSG);

	long int percent = strtol (argv[1], NULL, 10);
	if (! (0 <= percent && percent <= 50))
		errx (EX_USAGE, USAGE_MSG);

	FILE *fp = fopen (POWERCLAMP_PATH, "w");
	if (fp == NULL)
		err (EX_OSERR, "couldn't open " POWERCLAMP_PATH);

	int ret = fprintf (fp, "%li\n", percent);
	if (ret <= 0)
		err (EX_OSERR, "couldn't write to " POWERCLAMP_PATH);

	if (fclose (fp) != 0)
		err (EX_OSERR, "couldn't close " POWERCLAMP_PATH);

	return EXIT_SUCCESS;
}
