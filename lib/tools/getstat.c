#include <sys/cdefs.h>
#include <sys/types.h>
#include <sys/param.h>

#include <sys/resource.h>
#include <sys/sysctl.h>
#include <sys/vmmeter.h>
#include <vm/vm_param.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>

#define xsysctl(mib,destp,destlenp) \
	sysctl (mib##_mib, mib##_len, (destp), (destlenp), NULL, 0)

struct lavg { double one, five, fifteen; };

static char *get_time (char *, size_t);
static int16_t get_runq_len (void);
static struct lavg get_loadavg (void);
static u_int get_cpu_freq (void);
static int get_cpu_temp (u_int);

int
main (int argc __unused, char **argv __unused)
{
	do {
		char time_buf[9] = {};
		struct lavg lavg = get_loadavg ();
		printf (
			"%s %2hd %.2lf %.2lf %.2lf %4u %2d %2d %2d %2d\n",
			get_time (time_buf, 9),
			get_runq_len (),
			lavg.one, lavg.five, lavg.fifteen,
			get_cpu_freq (),
			get_cpu_temp (0), get_cpu_temp (1),
			get_cpu_temp (2), get_cpu_temp (3)
		);
	} while (sleep (1) == 0);

	return EXIT_SUCCESS;
}

static char *
get_time (char *buf, size_t bufsiz)
{
	time_t t = time (NULL);

	struct tm tm;
	localtime_r (&t, &tm);

	strftime (buf, bufsiz, "%H:%M:%S", &tm);

	return buf;
}

static int16_t
get_runq_len (void)
{
	int   vmtotal_mib[] = { CTL_VM, VM_TOTAL };
	u_int vmtotal_len   = nitems (vmtotal_mib);

	struct vmtotal vmtotal;
	memset (&vmtotal, 0, sizeof (vmtotal));
	size_t vmtotal_reslen = sizeof (vmtotal);
	if (xsysctl (vmtotal, &vmtotal, &vmtotal_reslen) != 0)
		err (EX_OSERR, "couldn't get vm.vmtotal");

	return vmtotal.t_rq;
}

static struct lavg
get_loadavg (void)
{
	int   loadavg_mib[] = { CTL_VM, VM_LOADAVG };
	u_int loadavg_len   = nitems (loadavg_mib);

	struct loadavg loadavg;
	memset (&loadavg, 0, sizeof (loadavg));
	size_t loadavg_reslen = sizeof (loadavg);

	if (xsysctl (loadavg, &loadavg, &loadavg_reslen) != 0)
		err (EX_OSERR, "couldn't get vm.loadavg");

	return (struct lavg) {
		.one     = (double)loadavg.ldavg[0] / (double)loadavg.fscale,
		.five    = (double)loadavg.ldavg[1] / (double)loadavg.fscale,
		.fifteen = (double)loadavg.ldavg[2] / (double)loadavg.fscale
	};
}

static u_int
get_cpu_freq (void)
{
	u_int freq;
	size_t freq_len = sizeof (freq);

	if (sysctlbyname ("dev.cpu.0.freq", &freq, &freq_len, NULL, 0) != 0)
		err (EX_OSERR, "couldn't get dev.cpu.0.freq");

	return freq;
}

static int
get_cpu_temp (u_int ncpu)
{
	u_int temp;
	size_t temp_len = sizeof (temp);
	char mib[32] = {};
	sprintf (mib, "dev.cpu.%d.temperature", ncpu);

	if (sysctlbyname (mib, &temp, &temp_len, NULL, 0) != 0)
		err (EX_OSERR, "couldn't get %s", mib);

	// deciKelvin?
	return (int) (((double)temp / 10.0) - 273.15);
}
