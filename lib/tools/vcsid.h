#if defined(__GNUC__) || defined(__INTEL_COMPILER)
#define __IDSTRING(name, string) __asm__(".ident\t\"" string "\"")
#else
#define __IDSTRING(name, string) static const char name[] __unused = string
#endif

#ifndef __VCSID
#if !defined(lint)
#define __VCSID(s) __IDSTRING (__CONCAT (__rcsid_, __LINE__), s)
#else
#define __VCSID(s) struct __hack
#endif
#endif
