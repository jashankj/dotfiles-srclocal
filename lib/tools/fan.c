/**
 * @brief userland command to set thinkpad_acpi fan
 * @author Jashank Jeremy <jashank@rulingia.com>
 * @date 2016-12-30
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <sysexits.h>
#include <unistd.h>

#ifndef __VCSID
# define __IDSTRING(name, string) __asm__(".ident\t\"" string "\"")
# define __VCSID(s) __IDSTRING (__CONCAT (__rcsid_, __LINE__), s)
#endif

__VCSID("$Id: fan.c,v 1.5 2016/12/30 06:41:15 jashank Exp $");

#define FAN_PATH "/proc/acpi/ibm/fan"
#define USAGE_MSG "usage: fan ([0-7] | auto | full-speed | disengaged)"

#define valid_levels(str,li)					\
	((0 <= li && li <= 7) ||					\
	 (strcmp (str, "auto") == 0) ||				\
	 (strcmp (str, "full-speed") == 0) ||		\
	 (strcmp (str, "disengaged") == 0))

int
main (int argc, char *argv[]) {
	if (geteuid () != 0)
		errx (EX_USAGE, "should be run as root...");

	if (argc != 2)
		errx (EX_USAGE, USAGE_MSG);

	char *level = argv[1];
	long int level_li = strtol (level, NULL, 10);
	if (! valid_levels (level, level_li))
		errx (EX_USAGE, USAGE_MSG);

	FILE *fp = fopen (FAN_PATH, "w");
	if (fp == NULL)
		err (EX_OSERR, "couldn't open " FAN_PATH);

	int ret = fprintf (fp, "level %s\n", level);
	if (ret <= 0)
		err (EX_OSERR, "couldn't write to " FAN_PATH);

	if (fclose (fp) != 0)
		err (EX_OSERR, "couldn't close " FAN_PATH);

	return EXIT_SUCCESS;
}
