# -*- makefile-bsdmake -*-

#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
#

#
# This tree builds libraries and binaries that get installed into my
# home directory, and to elsewhere on the system.  Set up so that, by
# default, we're targetting my home directory, and make it easy to put
# files elsewhere.
#

USER		?= jashank
GROUP		?= jeremy

ROOT_USER	?= root
ROOT_GROUP	?= wheel

PREFIX		?= ${HOME}
BINOWN		?= jashank
BINGRP		?= jeremy
BINDIR		?= ${PREFIX}/bin/${OSARCH}
LIBDIR		?= ${PREFIX}/lib/${OSARCH}
SHLIBDIR	?= ${LIBDIR}
INCLUDEDIR	?= ${PREFIX}/include
INCDIR		?= ${INCLUDEDIR}
MANDIR		?= ${PREFIX}/share/man
MANTARGET	?= man
DOCDIR		?= ${PREFIX}/share/doc
NLSDIR		?= ${PREFIX}/share/nls


# Support building on Linux ---
.if ${.MAKE.OS} == "Linux"
. include "Makefile.inc.linux"
.endif
