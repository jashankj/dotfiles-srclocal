Improve Sphinx's LaTeX output format.

Index: src/base/sphinx/sphinx/writers/latex.py
===================================================================
--- src.orig/base/sphinx/sphinx/writers/latex.py
+++ src/base/sphinx/sphinx/writers/latex.py
@@ -1469,8 +1469,12 @@ class LaTeXTranslator(SphinxTranslator):
             ctx = r'\phantomsection'
             for node_id in node['ids']:
                 ctx += self.hypertarget(node_id, anchor=False)
-        ctx += r'}'
-        self.body.append(r'\sphinxlineitem{')
+        if isinstance(node, nodes.field_name):
+            otag, ctag = r'\item[', r']'
+        else:
+            otag, ctag = r'\sphinxlineitem{', r'}'
+        self.body.append(otag)
+        ctx += ctag
         self.context.append(ctx)
 
     def depart_term(self, node: Element) -> None:
@@ -1490,12 +1494,12 @@ class LaTeXTranslator(SphinxTranslator):
         self.body.append(CR)
 
     def visit_field_list(self, node: Element) -> None:
-        self.body.append(r'\begin{quote}\begin{description}' + CR)
+        self.body.append(r'\begin{description}[leftmargin=!,labelwidth=6em]' + CR)
         if self.table:
             self.table.has_problematic = True
 
     def depart_field_list(self, node: Element) -> None:
-        self.body.append(r'\end{description}\end{quote}' + CR)
+        self.body.append(r'\end{description}' + CR)
 
     def visit_field(self, node: Element) -> None:
         pass
@@ -1523,6 +1527,10 @@ class LaTeXTranslator(SphinxTranslator):
             # don't insert blank line, if the paragraph is second child of a footnote
             # (first one is label node)
             pass
+        elif isinstance(node.parent, nodes.field_body):
+            # We're in a field list; don't force a break.
+            self.body.extend([CR, r'\par' + CR])
+            pass
         else:
             # the \sphinxAtStartPar is to allow hyphenation of first word of
             # a paragraph in narrow contexts such as in a table cell
