/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
 */

use std::process::Stdio;

use clap::Parser;
use color_eyre::eyre::{Result, eyre};
use tokio::{
	fs,
	io::{self, AsyncRead, AsyncReadExt},
	process::Command,
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
	#[clap(long)]
	stdin_file:  Option<String>,
	#[clap(long)]
	stdout_file: Option<String>,
	#[clap(long)]
	stderr_file: Option<String>,
	#[clap(long = "exit", default_value_t = 0)]
	exitcode: i32,
	#[clap(long)]
	silent: bool,
	#[clap()]
	argv: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<()>
{
	color_eyre::install()?;
	let args = Args::parse();
//	println!("{:?}", &args);

	async fn open (f: &Option<String>) -> Result<Box<dyn AsyncRead + Unpin>>
	{
		if let Some(f) = f {
			Ok(Box::new(fs::File::open(f).await?))
		} else {
			Ok(Box::new(io::empty()))
		}
	}

 	let mut stdin_source    = open(&args.stdin_file).await?;

 	let mut stdout_expected = open(&args.stdout_file).await?;
	let mut stdout_actual: Vec<u8> = Vec::new();
	let mut stdout_expect: Vec<u8> = Vec::new();
	stdout_expected.read_to_end(&mut stdout_expect).await?;

 	let mut stderr_expected = open(&args.stderr_file).await?;
	let mut stderr_actual: Vec<u8> = Vec::new();
	let mut stderr_expect: Vec<u8> = Vec::new();
	stderr_expected.read_to_end(&mut stderr_expect).await?;

	let (x, xs) = args.argv.split_first().unwrap();
	let mut child = Command::new(x).args(xs)
		.stdin(Stdio::piped())
		.stdout(Stdio::piped())
		.stderr(Stdio::piped())
		.spawn()?;
	let mut stdin  = child.stdin .take().unwrap();
	let mut stdout = child.stdout.take().unwrap();
	let mut stderr = child.stderr.take().unwrap();

	let (_, _, _, status) = tokio::join!(
		io::copy(&mut stdin_source, &mut stdin),
		stdout.read_to_end(&mut stdout_actual),
		stderr.read_to_end(&mut stderr_actual),
		child.wait(),
	);

	let mut retval = 0;

	if let Some(code) = status.unwrap().code() {
		if code != args.exitcode {
			if !args.silent {
				println!("exit: expected {}, got {}", args.exitcode, code);
			}
			retval |= 0b001;
		}
	}

	fn express_diff (
		silent: bool,
		ident:  &str,
		expect: &[u8],
		actual: &[u8],
		bit:    i32
	) -> i32
	{
		let differences = diff::slice(expect, actual);
		let mut x = false;

		if differences.iter().find(|x| {
			if let diff::Result::Both(_, _) = x { false } else { true }
		}).is_some() {
			if !silent {
				println!("{}:", ident);
			}
			for diff in differences {
				match diff {
					diff::Result::Left(l)    => {
						if !silent { println!("  -{:02x}", l); } x = true; },
					diff::Result::Both(l, _) =>
						if !silent { println!("   {:02x}", l); },
					diff::Result::Right(r)   => {
						if !silent { println!("  +{:02x}", r); } x = true; },
				}
			}
		}

		if x { bit } else { 0 }
	}

	retval |= express_diff(args.silent, "stdout", &stdout_expect, &stdout_actual, 0b010);
	retval |= express_diff(args.silent, "stderr", &stderr_expect, &stderr_actual, 0b100);

	if retval == 0 {
		Ok(())
	} else {
		println!("");
		Err(eyre!("{:03b}", retval))
	}
}
