#!/usr/bin/env perl
#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
#

use v5;
use strict;
use warnings;
use feature 'say';
use feature 'switch';
no warnings 'experimental::smartmatch';
use threads;
use threads::shared;

use Data::Dumper;
use Getopt::Long;
use IO::File;
use IO::Handle;
use IO::String;
use IPC::Open3;
use Symbol 'gensym';
use Text::Diff;

my ($stdin_str,  $stdin_file);
my ($stdout_str, $stdout_file);
my ($stderr_str, $stderr_file);
my $exitcode = 0;

GetOptions (
	'stdin=s'       => \$stdin_str,
	'stdin-file=s'  => \$stdin_file,
	'stdout=s'      => \$stdout_str,
	'stdout-file=s' => \$stdout_file,
	'stderr=s'      => \$stderr_str,
	'stderr-file=s' => \$stderr_file,
	'exit=i'        => \$exitcode,
) or die "usage: TEST1 [--stdin-file=FILE] [--stdout-file=FILE] [--stderr-file=FILE] [--exit=N] -- PROGRAM...\n";

my $stdin;
if ($stdin_file) {
	$stdin = IO::File->new($stdin_file, 'r')
	  or die "couldn't open $stdin_file: $!";
} elsif ($stdin_str) {
	$stdin = IO::String->new($stdin_str);
} else {
	$stdin = IO::String->new('');
}

my $stdout_expected;
if ($stdout_file) {
	$stdout_expected = IO::File->new($stdout_file, 'r')
	  or die "couldn't open $stdout_file: $!";
} elsif ($stdout_str) {
	$stdout_expected = IO::String->new($stdout_str);
} else {
	$stdout_expected = IO::String->new('');
}
my $stdout_actual :shared = IO::String->new();

my $stderr_expected;
if ($stderr_file) {
	$stderr_expected = IO::File->new($stderr_file, 'r')
	  or die "couldn't open $stderr_file: $!";
} elsif ($stderr_str) {
	$stderr_expected = IO::String->new($stderr_str);
} else {
	$stderr_expected = IO::String->new('');
}
my $stderr_actual :shared = IO::String->new();

sub io_feeder {
	my ($io_in, $io_out) = @_;
	while (my $c = $io_in->getc()) {
		$io_out->print($c);
	}
	$io_in->close;
	$io_out->close;
}

my $tin :shared;
my $tout :shared;
my $terr :shared;
my $tpid = open3($tin, $tout, $terr, @ARGV)
  or die "couldn't run '".(join ' ', @ARGV)."': $!";


my $in  = threads->create(\&io_feeder, $stdin, $tin);
my $out = threads->create(\&io_feeder, $tout,  $stdout_actual);
my $err = threads->create(\&io_feeder, $terr,  $stderr_actual);

waitpid $tpid, 0;
my $texit = $? >> 8;

$in->join()  if ($in->is_joinable());
$out->join() if ($out->is_joinable()); $stdout_actual->pos(0);
$err->join() if ($err->is_joinable()); $stderr_actual->pos(0);

my $dout = diff $stdout_actual->string_ref, $stdout_expected->string_ref, { STYLE => "Unified" };
my $derr = diff $stderr_actual->string_ref, $stderr_expected->string_ref, { STYLE => "Unified" };

say Dumper $stdout_actual->string_ref;
say Dumper $stdout_expected->string_ref;
say Dumper \$dout;

say Dumper $stderr_actual->string_ref;
say Dumper $stderr_expected->string_ref;
say Dumper \$derr;

