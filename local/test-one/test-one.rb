#!/usr/bin/env ruby
# typed: ignore

#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
#

require 'optparse'
require 'stringio'
require 'tracer'

require 'rubygems'
require 'diffy'
# require 'sorbet-runtime'
require 'subprocess'

# Tracer.on

# extend T::Sig

# sig { params(argv: T::Array[T.untyped])
#         .returns([ T::Hash[Symbol, T.untyped], T::Array[String] ]) }
def parse_args(argv)
  options = {}
  parser = OptionParser.new do |opts|
    opts.banner = "usage: TEST1 [options] -- PROGRAM...";

    %w[in out err].each do |x|
      opts.on("--std#{x}=STRING", String, "standard #{x} from a string") do |s|
        options["std#{x}_str".to_sym] = s
      end
      opts.on("--std#{x}-file=FILE", String, "standard #{x} from a file") do |s|
        options["std#{x}_file".to_sym] = s
      end
    end

    opts.on("--exit=NUM", Integer, "expected exit code") do |k|
      options[:exitcode] = k
    end
  end

  parser.parse!(argv)
  return [options, argv.map(&:to_s)]
end

# sig { params(file: T.nilable(String), str: T.nilable(String))
#         .returns(T.any(IO, StringIO)) }
def init_iodata(file, str)
  if file then
    File.open(file, 'r')
  elsif str then
    StringIO.new(str, 'r')
  else
    StringIO.new('', 'r')
  end
end

# sig { params(options: T::Hash[Symbol, T.untyped]).returns(T.any(IO, StringIO)) }
def init_stdin(options)
  init_iodata options[:stdin_file], options[:stdin_str]
end

# sig { params(options: T::Hash[Symbol, T.untyped]).returns(T.any(IO, StringIO)) }
def init_stdout(options)
  init_iodata options[:stdout_file], options[:stdout_str]
end

# sig { params(options: T::Hash[Symbol, T.untyped]).returns(T.any(IO, StringIO)) }
def init_stderr(options)
  init_iodata options[:stderr_file], options[:stderr_str]
end

# sig { params(argv: T::Array[T.untyped]).void }
def main(argv)
  options, t_args = parse_args argv
  stdin           = init_stdin options
  stdout_expected = init_stdout options
  stdout_actual   = StringIO.new
  stderr_expected = init_stderr options
  stderr_actual   = StringIO.new

  in_r,  in_w  = IO.pipe
  out_r, out_w = IO.pipe
  err_r, err_w = IO.pipe

  tproc = Subprocess::Process.new(
    t_args,
    stdin:  in_r,
    stdout: out_w,
    stderr: err_w,
  )

  t_in  = Thread.new { io_feeder stdin, in_w }
  t_out = Thread.new { io_feeder out_r, stdout_actual }
  t_err = Thread.new { io_feeder err_r, stderr_actual }
  t_in[:id] = "in"; t_out[:id] = "out"; t_err[:id] = "err"

  [in_r, out_w, err_w].map(&:close)
  [t_in, t_out, t_err].each {|t| t.run }
  texit = tproc.wait
  [t_in, t_out, t_err].each {|t| t. }

  stdout_actual   = stdout_actual.read
  stdout_expected = stdout_expected.read
  stderr_actual   = stderr_actual.read
  stderr_expected = stderr_expected.read

  dout = Diffy::Diff.new stdout_actual, stdout_expected
  derr = Diffy::Diff.new stderr_actual, stderr_expected

  p stdout_actual
  p dout
  p stderr_actual
  p derr

  # my $dout = diff $stdout_actual->string_ref, $stdout_expected->string_ref, { STYLE => "Unified" };
  # my $derr = diff $stderr_actual->string_ref, $stderr_expected->string_ref, { STYLE => "Unified" };

  # say Dumper $stdout_actual->string_ref;
  # say Dumper $stdout_expected->string_ref;
  # say Dumper \$dout;

  # say Dumper $stderr_actual->string_ref;
  # say Dumper $stderr_expected->string_ref;
  # say Dumper \$derr;
end

main(ARGV) if __FILE__ == $0
