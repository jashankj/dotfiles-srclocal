#!/usr/bin/env runhaskell
{--
:Module:	ccat
:Description:	cat with comments.
:Stability:	experimental
:Portability:	*nix

Based on ``ccat`` (previously Shell + Sed).
--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Main where

import           Control.Monad (forM_)
import qualified Data.ByteString.Lazy as LBS
import           System.Environment (getArgs)
import           Text.RE.TDFA
import           Text.RE.Tools

main :: IO ()
main = getArgs >>= main'
  where
    main' [] =                  sed ccat "-" "-"
    main' xs = forM_ xs $ \f -> sed ccat  f  "-"

ccat :: Edits IO RE LBS.ByteString
ccat =
  Select
    [ Template $ SearchReplace [reBS|#.*|] mempty
    , LineEdit [reBS|^[[:space:]]*$|] $ \_ _ -> pure Delete ]
