#!/usr/bin/env runhaskell
{--
:Module:	statusbard
:Description:	fill out my i3 status bar
:Stability:	experimental
:Portability:	*nix

Based on ``statusbard.rb``.
--}

{-# LANGUAGE ApplicativeDo #-}

module Main
where

type Alist k v = [(k, v)]

------------------------------------------------------------------------

-- | A mapping from a client name to a list of blocks to render to them.
blocks :: Alist String [String]
blocks =
  [ -- alyzon LHS screen: task status
    ( "alyzon_left"
    , [ "todoist"
      , "loadavg"
      , "date" ])

    --- alyzon RHS screen: system status
  , ( "alyzon_right"
    , [ "ifwatch_bridge0"
      , "ifwatch_em0"
      , "ifwatch_msk0"
      , "ifwatch_tun0"
      , "thermal"
      , "cpufreq"
      , "loadavg"
      , "date" ])

    --- jaenelle
  , ( "jaenelle"
    , [ "ifwatch_bond0"
      , "ifwatch_enp0s25"
      , "wlwatch_wlp3s0"
      , "ifwatch_bnep0"
      , "ifwatch_tun0"
      , "ifwatch_tun1"
      , "battery"
      , "thermal_jaenelle"
      , "cpufreq_jaenelle"
      , "loadavg"
      , "date" ])

    --- lisbon --- TODO add ifwatch/wlwatch
  , ( "lisbon"
    , [ "ifwatch_bond0"
      , "ifwatch_enp2s0f0"
      , "wlwatch_wlp3s0"
      , "battery"
      , "thermal_lisbon"
      , "cpufreq_lisbon"
      , "loadavg"
      , "date" ])

    --- TODO: add tasks
  , ( "lisbon_DisplayPort_4"
    , [ "ifwatch_bond0"
      , "ifwatch_enp2s0f0"
      , "wlwatch_wlp3s0"
      , "date" ])
    --- TODO: add tasks
  , ( "lisbon_DisplayPort_3"
    , [ "thermal_lisbon"
      , "cpufreq_lisbon"
      , "battery"
      , "loadavg"
      , "date" ])
    --- TODO: review?
  , ( "lisbon_DisplayPort_2"
    , [ "ifwatch_bond0"
      , "ifwatch_enp2s0f0"
      , "wlwatch_wlp3s0"
      , "battery"
      , "thermal_lisbon"
      , "cpufreq_lisbon"
      , "loadavg"
      , "date" ])

    --- TODO: review
  , ( "lisbon_HDMI_A_0"
    , [ "date" ])

    --- menolly
  , ( "menolly"
    , [ "ifwatch_eth0"
      , "thermal_menolly"
      , "cpufreq_menolly"
      , "loadavg"
      , "date" ])

    --- wedge
  , ( "wedge"
    , [ "ifwatch_eno1"
      , "thermal_wedge"
      , "cpufreq_wedge"
      , "loadavg"
      , "date" ])

    --- yarn
  , ( "yarn"
    , [ "wlwatch_wlo0"
      , "thermal_yarn"
      , "cpufreq_yarn"
      , "battery"
      , "loadavg"
      , "date" ])
  ]

_USE_DBUS   = True
_USE_NOTIFY = False
_NOTIFY_ON  = "wedge"
_TIMEOUT    = 1.0
_DEBUG      = "TIMEOUT" -- True

-- _SBD_SOCKET  = (getenv "HOME") ++ "/.cache/statusbard.sock"
-- _SBD_PIDFILE = (getenv "HOME") ++ "/.cache/statusbard.pid"

------------------------------------------------------------------------
-- Debugging configuration

------------------------------------------------------------------------
-- Colour names

_COLOURS :: Alist String String
_COLOURS =
  [ ("fg",    "#ffffff")
  , ("bg",    "#000000")
  , ("red",   "#ff0000")
  , ("green", "#00ff00")
  ]

------------------------------------------------------------------------
-- Useful symbol names from FontAwesome.

------------------------------------------------------------------------
-- D-Bus support.

------------------------------------------------------------------------
-- Notifications.

------------------------------------------------------------------------
-- Block processes.

------------------------------------------------------------------------
-- Network interfaces.

------------------------------------------------------------------------
-- Wireless interfaces.

------------------------------------------------------------------------
-- Battery functions.

------------------------------------------------------------------------
-- Thermal functions.

------------------------------------------------------------------------
-- System core frequency functions.

------------------------------------------------------------------------
-- System load average.

------------------------------------------------------------------------
-- Block list!

------------------------------------------------------------------------
-- The server loop.

------------------------------------------------------------------------
-- Request handling.

------------------------------------------------------------------------
-- Block management.

------------------------------------------------------------------------
-- Socket management.

------------------------------------------------------------------------
-- PID-file management.

------------------------------------------------------------------------
-- Performance reporting.


------------------------------------------------------------------------

main :: IO ()
main = do
  undefined

------------------------------------------------------------------------

{-
---- i3blocks syntax
--
-- full_text: The full_text will be displayed by i3bar on the status
-- line. This is the only required key.
--
-- short_text: Where appropriate, the short_text (string) entry should
-- also be provided.
--
-- color: To make the current state of the information easy to spot,
-- colors can be used.  Colors are specified in hex (like in HTML),
-- starting with a leading hash sign. For example, #ff0000 means red.
--
-- background: Overrides the background color for this block.
--
-- border: Overrides the border color for this block.
--
-- min_width: The minimum width (in pixels) of the block; the value can
-- also be a string, and the width of the text given determines the
-- minimum width of the block.
--
-- align: Align to the center, right or left (default) of the block.
--
-- name and instance: Every block should have a unique name (string)
-- entry so that it can be easily identified in scripts which process
-- the output. i3bar completely ignores the name and instance
-- fields. Make sure to also specify an instance (string) entry where
-- appropriate. For example, the user can have multiple disk space
-- blocks for multiple mount points.
--
-- urgent: A boolean: whether the current value is urgent.
--
-- separator: A boolean which specifies whether a separator line should
-- be drawn after this block. The default is true, meaning the
-- separator line will be drawn. Note that if you disable the separator
-- line, there will still be a gap after the block, unless you also use
-- separator_block_width.
--
-- separator_block_width: The amount of pixels to leave blank after the
-- block. In the middle of this gap, a separator line will be drawn
-- unless separator is disabled. Normally, you want to set this to an
-- odd value (the default is 9 pixels), since the separator line is
-- drawn in the middle.
--
-- markup: A string that indicates how the text of the block should be
-- parsed. Set to "pango" to use Pango markup. Set to "none" to not use
-- any markup (default).
--}
