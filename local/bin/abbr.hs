#!/usr/bin/env runhaskell
{--
:Module:	abbr
:Description:	print an abbreviation
:Stability:	experimental
:Portability:	*nix

Based on ``abbr`` (previously Shell + clep).
--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C
import qualified Data.Char as Char
import           Data.Functor
import           System.Environment (getArgs, getEnv)
import           System.FilePath
import qualified System.IO as IO

import qualified UIO

main :: IO ()
main = getArgs >>= main'

main' :: Foldable t => t String -> IO ()
main' xs
  | null xs = error "usage: abbr <term>..."
  | otherwise = do
      inf <- abbrevFile >>= flip IO.openFile IO.ReadMode
      main'' inf IO.stdout xs

main'' :: Foldable t => IO.Handle -> IO.Handle -> t String -> IO ()
main'' hi ho xs = do
  abbrevs <- UIO.loadTable '\t' '\n' hi
  mapM_ (mapM_ (C.hPutStrLn ho . tabulate) . flip select abbrevs . C.pack) xs

select :: ByteString -> [[ByteString]] -> [[ByteString]]
select x = filter (match x . head)

tabulate :: [ByteString] -> ByteString
tabulate = C.intercalate $ C.singleton '\t'

match :: ByteString -> ByteString -> Bool
match a b  =  clean a == clean b
  where clean = C.map Char.toLower . C.strip

abbrevFile :: IO FilePath
abbrevFile = getEnv "HOME" <&> (</> ".abbr")
