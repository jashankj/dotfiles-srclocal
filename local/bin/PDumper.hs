#!/usr/bin/env runhaskell
{--
:Module:	PDumper.hs
:Description:	an Emacs portable-dumper file
:Stability:	experimental
:Portability:	*nix
:SPDX-License-Identifier: GPL-3.0-or-later

  Portions derived from GNU Emacs, which is copyright
  Copyright (C) Free Software Foundation, Inc.
  Contains constants defined in:
    + //emacs/src/pdumper.c
      Copyright (C) 2018-2022 Free Software Foundation, Inc.
    + //emacs/src/pdumper.h
      Copyright (C) 2016, 2018-2022 Free Software Foundation, Inc.

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StandaloneDeriving #-}

module PDumper
  ( Explicable(..), Undumpable(..)
  , DumpOff
  , DumpRelocType(..)
  , EmacsRelocType -- (..)
  , EmacsReloc -- (..)
  , DumpTableLocator(..)
  , DumpReloc -- (..)
  , RelocPhase(..)
  , DumpHeader(..)
  )
where

import           Control.Applicative (Alternative(..), liftA2)
import           Data.Bits
import qualified Data.Attoparsec.ByteString as P
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Conversion as Conv
import           Data.Int
import           Data.Word

class Explicable a where
  explain :: a -> String
class Undumpable a where
  undump :: P.Parser a

------------------------------------------------------------------------

newtype DumpOff = DumpOff Int32

instance Undumpable DumpOff where
  undump = Conv.parser >>= pure . DumpOff

------------------------------------------------------------------------

-- # src/pdumper.c 114..118
_DUMP_MAGIC = C.pack
  [ 'D', 'U', 'M', 'P', 'E', 'D'
  , 'G', 'N', 'U'
  , 'E', 'M', 'A', 'C', 'S' ]

-- # src/pdumper.c 173..193
data DumpRelocType
  = RELOC_DUMP_TO_EMACS_PTR_RAW
    -- ^ dump_ptr = dump_ptr + emacs_basis()
  | RELOC_DUMP_TO_DUMP_PTR_RAW
    -- ^ dump_ptr = dump_ptr + dump_base
  | RELOC_NATIVE_COMP_UNIT
    -- ^ dump_mpz = [rebuild bignum]
  | RELOC_NATIVE_SUBR
  | RELOC_BIGNUM
  | RELOC_DUMP_TO_DUMP_LV
    -- ^ dump_lv = make_lisp_ptr (dump_lv + dump_base, type - RELOC_DUMP_TO_DUMP_LV)
    --   (Special case for symbols: make_lisp_symbol)
    --   Must be second-last.
  | RELOC_DUMP_TO_EMACS_LV -- = RELOC_DUMP_TO_DUMP_LV + 8
    -- ^ dump_lv = make_lisp_ptr (dump_lv + emacs_basis(), type - RELOC_DUMP_TO_DUMP_LV)
    --   (Special case for symbols: make_lisp_symbol.)
    --   Must be last.

data EmacsRelocType

data EmacsReloc

data DumpTableLocator = DumpTableLocator
  { _dtl_offset :: DumpOff
    -- ^ Offset in dump, in bytes, of the first entry in the dump table.
  , _dtl_nr_entries :: DumpOff
    -- ^ Number of entries in the dump table.  We need an explicit end
    --   indicator (as opposed to a special sentinel) so we can
    --   efficiently binary search over the relocation entries.
  }

instance Undumpable DumpTableLocator where
  undump = do
    offset     <- undump
    nr_entries <- undump
    return $ DumpTableLocator
      { _dtl_offset = offset
      , _dtl_nr_entries = nr_entries
      }

data DumpReloc

-- # src/pdumper.c 325..337
-- | To be used if some order in the relocation process has to be
--   enforced.
data RelocPhase
  = EARLY_RELOCS
    -- ^ First to run.  Place every relocation with no dependency here.
  | LATE_RELOCS
    -- ^ Late and very late relocs are relocated at the very last after
    --   all hooks has been run.  All lisp machinery is at disposal
    --   (memory allocation allowed too).
  | VERY_LATE_RELOCS
_RELOC_NUM_PHASES = 3

-- | Format of an Emacs dump file.  All offsets are relative to the
--   beginning of the file.  An Emacs dump file is coupled to exactly
--   the Emacs binary that produced it, so details of alignment and
--   endianness are unimportant.
--
--   An Emacs dump file contains the contents of the Lisp heap.  On
--   startup, Emacs can start faster by mapping a dump file into memory
--   and using the objects contained inside it instead of performing
--   initialization from scratch.
--
--   The dump file can be loaded at arbitrary locations in memory, so it
--   includes a table of relocations that let Emacs adjust the pointers
--   embedded in the dump file to account for the location where it was
--   actually loaded.
--
--   Dump files can contain pointers to other objects in the dump file
--   or to parts of the Emacs binary.
data DumpHeader = DumpHeader
  { _dh_magic :: ByteString
    -- ^ File type magic.

  , _dh_fingerprint :: ByteString
    -- ^ Associated Emacs binary.

  , _dh_dump_relocs_dtl :: [DumpTableLocator]
    -- ^ Relocation table for the dump file.
    --   Each entry is a struct dump_reloc.

  , _dh_object_starts_dtl :: DumpTableLocator
    -- ^ "Relocation" table we abuse to hold information about the
    --   location and type of each lisp object in the dump.  We need for
    --   pdumper_object_type and ultimately for conservative GC
    --   correctness.

  , _dh_emacs_relocs_dtl :: DumpTableLocator
  , _dh_emacs_relocs :: [EmacsReloc]
    -- ^ Relocation table for Emacs.
    --   Each entry is a struct emacs_reloc.

  , _dh_discardable_start :: Int
    -- ^ Start of sub-region of hot region that we can discard after
    --   load completes.  The discardable region ends at cold_start.
    --   This region contains objects that we copy into the Emacs image
    --   at dump-load time.

  , _dh_cold_start :: Int
    -- ^ Start of the region that does not require relocations and that
    --   we expect never to be modified.  This region can be
    --   memory-mapped directly from the backing dump file with the
    --   reasonable expectation of taking few copy-on-write faults.
    --
    --   For correctness, however, this region must be modifible, since
    --   in rare cases it is possible to see modifications to these
    --   bytes.  For example, this region contains string data, and it's
    --   technically possible for someone to ASET a string character
    --   (although nobody tends to do that).
    --
    --   The start of the cold region is always aligned on a page
    --   boundary.

  , _dh_hash_list :: Int
    -- ^ Offset of a vector of the dumped hash tables.
  }

deriving instance Show DumpOff
deriving instance Show DumpRelocType
deriving instance Show EmacsRelocType
deriving instance Show EmacsReloc
deriving instance Show DumpTableLocator
deriving instance Show DumpReloc
deriving instance Show RelocPhase
deriving instance Show DumpHeader

manyN :: (Alternative f, Eq n, Num n) => n -> f a -> f [a]
manyN n p
  | n == 0    = pure []
  | otherwise = liftA2 (:) p (manyN (n - 1) p)

instance Undumpable DumpHeader where
  undump = do
    magic           <- P.take 16
    fingerprint     <- P.take 32
    dump_relocs_dtl <- manyN _RELOC_NUM_PHASES undump
    return $ DumpHeader
      { _dh_magic             = magic
      , _dh_fingerprint       = fingerprint
      , _dh_dump_relocs_dtl   = dump_relocs_dtl
      , _dh_object_starts_dtl = undefined
      , _dh_emacs_relocs_dtl  = undefined
      , _dh_emacs_relocs      = undefined
      , _dh_discardable_start = undefined
      , _dh_cold_start        = undefined
      , _dh_hash_list         = undefined
      }

{-
sub read {
	my ($class, $fp, $offset) = @_;
	my $h = {};

	$h->{'magic'}       = field_read($fp, 'dump_header', 'magic', $offset, 16);
	die 'dump_header::read: magic' unless defined $h->{'magic'};

	$h->{'fingerprint'} = field_read($fp, 'dump_header', 'fingerprint', undef, 32);
	die 'dump_header::read: fingerprint' unless defined $h->{'fingerprint'};

	$h->{'dump_relocs_dtl'} = [];
	for (my $i = 0; $i < pdump::RELOC_NUM_PHASES; $i++) {
		my $dtl = pdump::dump_table_locator->read($fp, undef);
		die "dump_header::read: dump_relocs[$i]" unless defined $dtl;
		$h->{'dump_relocs_dtl'}->[$i] = $dtl;
	}
	# (struct dump_table_locator [RELOC_NUM_PHASES])

	my $osdtl = pdump::dump_table_locator->read($fp, undef);
	die "dump_header::read: object_starts_dtl" unless defined $osdtl;
	$h->{'object_starts_dtl'} = $osdtl; # (struct dump_table_locator)

	my $erdtl = pdump::dump_table_locator->read($fp, undef);
	die "dump_header::read: emacs_relocs dtl" unless defined $erdtl;
	$h->{'emacs_relocs_dtl'} = $erdtl; # (struct dump_table_locator)

	$h->{'emacs_relocs'} = [];
	for (my $i = 0; $i < $erdtl->nr_entries; $i++) {
		my $at = $erdtl->offset + ($i * pdump::sizeof::emacs_reloc);
		my $er = pdump::emacs_reloc->read($fp, $at);
		die "dump_header::read: emacs_relocs[$i]" unless defined $er;
		$h->{'emacs_relocs'}->[$i] = $er;
	}

	my $ds = dump_off_read($fp, undef);
	die "dump_header::read: discardable_start" unless defined $ds;
	$h->{'discardable_start'} = $ds; # (dump_off)

	my $cs = dump_off_read($fp, undef);
	die "dump_header::read: cold_start" unless defined $ds;
	$h->{'cold_start'} = $cs; # (dump_off)

	my $hl = dump_off_read($fp, undef);
	die "dump_header::read: hash_list" unless defined $ds;
	$h->{'hash_list'} = $hl; # (dump_off)

	return $class->new(%$h);
}
-}
