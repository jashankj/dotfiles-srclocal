#!/usr/bin/env runhaskell
{--
:Module:	timestamped
:Description:	add a timestamp to streaming messages
:Stability:	experimental
:Portability:	*nix

Based on ``timestamped`` (previously Shell + Awk).
--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Time ( FormatTime, defaultTimeLocale, formatTime
                 , getCurrentTime )
import System.IO ( stdin, stdout )
import qualified UIO

main :: IO ()
main = UIO.interactM stdin stdout timestamped

timestamped :: String -> IO String
timestamped x = (++ "\t" ++ x) . strftime "%F %T" <$> getCurrentTime

strftime :: FormatTime t => String -> t -> String
strftime = formatTime defaultTimeLocale
