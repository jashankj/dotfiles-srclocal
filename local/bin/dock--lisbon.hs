#!/usr/bin/env runhaskell
{--
:Module:	dock.lisbon
:Description:	docking operations on `lisbon'
:Stability:	experimental
:Portability:	*nix

:Synopsis:	dock (up [<L> <C> <R>] | down)

.. TODO(jashankj):: Find a nice XRandR shim.

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Monad (void)
import           Debug.Trace (trace)
import           System.Environment
import           System.Process.Typed

import           UProc (run, xrandr)

main :: IO ()
main = getArgs >>= main'

main' :: [String] -> IO ()
main' ["up", l, c, r] =
  let dll = "eDP"
      dl  = "DisplayPort-" ++ l
      dc  = "DisplayPort-" ++ c
      dr  = "DisplayPort-" ++ r
  in
    void $ run $
    xrandr' [ "--output", dll,                   "--rotate", "normal", "--auto"
           , "--output", dl, "--right-of", dll, "--rotate", "left",   "--auto"
           , "--output", dc, "--right-of", dl,  "--rotate", "left",   "--auto"
           , "--output", dr, "--right-of", dc,  "--rotate", "normal", "--auto" ]
main' ["up"] = main' ["up", "4", "3", "2"]
main' ["down"] =
  void $ run $ xrandr' $
  concatMap (\o -> ["--output", o, "--off"]) $
  ["DisplayPort-" ++ show x | x <- [(0::Integer)..7]]
main' _ = error "usage: dock (up [<L> <C> <R>] | down)"

xrandr' :: [String] -> ProcessConfig () () ()
xrandr' args = trace (unwords ("xrandr" : args)) $ xrandr args
