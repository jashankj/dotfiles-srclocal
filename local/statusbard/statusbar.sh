#!/bin/sh

#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2018-2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

if [ "X$1" = "X" ]
then
	echo "usage: $(basename $0) (alyzon_left|alyzon_right|jaenelle|menolly|wedge)"
	exit 64 #EX_USAGE
fi
what="$1"

echo '{"version":1}['
while true
do
	echo "$what" \
	| nc -U ~/.cache/statusbard.sock
	# | nc localhost 2018
	# | json2yaml; echo
	sleep 1
done
echo ']'
