#!/bin/sh

# PROVIDE:	amd17h_c6fix
# REQUIRE:	root mountcritlocal
# KEYWORD:	nojail
# BEFORE:	SERVERS

#
# Add the following line to /etc/rc.conf to enable flow-capture:
# amd17h_c6fix_enable (bool):	Set it to "YES" to update microcode on startup
#				Set to "NO" by default.
# amd17h_c6fix_cpus (str):	A list of cpus to update on startup, or "ALL" for all.
#				Example: amd17h_c6fix_cpus="0 1"
#				Set to "ALL" by default. 

. /etc/rc.subr

name="amd17h_c6fix"
rcvar=amd17h_c6fix_enable
stop_cmd=":"
start_precmd="amd17h_c6fix_prepare"
start_cmd="amd17h_c6fix_start"
requires_modules="cpuctl"

amd17h_c6fix_prepare()
{
	if ! kldstat -q -m cpuctl
	then
		if ! kldload cpuctl > /dev/null 2>&1
		then
			warn "Can't load cpuctl module."
			return 1
		fi
	fi
}

amd17h_c6fix_start()
{
	echo "Disabling broken C6 state on AMD 17h CPU..."
	if [ "${amd17h_c6fix_cpus}" = "ALL" ]
	then
		ncpu=`/sbin/sysctl -n hw.ncpu`
		cpus=`jot ${ncpu} 0`;
	else
		cpus=${amd17h_c6fix_cpus}
	fi

	for i in ${cpus}
	do
		/usr/local/sbin/amd17h-c6fix /dev/cpuctl${i} |
			logger -p daemon.notice -t amd17h_c6fix || \
			(echo "amd17h_c6fix failed." && exit 1)
	done
}

load_rc_config $name
: ${amd17h_c6fix_enable="NO"}
: ${amd17h_c6fix_cpus="ALL"}
run_rc_command "$1"
