/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2022 Jashank Jeremy <jashank at rulingia.com.au>
 */

/**
 * @file	~/src/local/amd17h-c6fix/amd17h-c6fix.c
 * @brief	disable C6 states on an AMD Ryzen CPU
 * @author	Jashank Jeremy &lt;jashank at rulingia.com.au&gt;
 * @date	2022-10-09
 */

#include <sys/cdefs.h>
#include <sys/param.h>

#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include <sys/cpuctl.h>
#include <sys/ioctl.h>

/* __RCSID("$Id$"); */

enum { MSR_PKG = 0xC0010292ULL, MSR_CORE = 0xC0010296ULL };

static bool fix_one_cpu (const char *);
static bool fix_c6state_pkg (const char *, const int);
static bool fix_c6state_core (const char *, const int);
static bool rdmsr (const char *, const int, const uint32_t, uintptr_t *);
static bool wrmsr (const char *, const int, const uint32_t, const uintptr_t);

static inline uintptr_t __pure2 BIT (uint8_t idx) { return (1ULL << idx); }

int
main (const int argc, const char *argv[])
{
	if (argc != 2)
		errx (EX_USAGE, "usage: amd17h-c6fix </dev/cpuctlN>");
	if (! fix_one_cpu (argv[1]))
		errx (EX_OSERR, "fix_one_cpu(dev=%s)", argv[1]);
	return EXIT_SUCCESS;
}

static bool
fix_one_cpu (const char *dev)
{
	bool out = true;
	int fd;
	if ((fd = open (dev, O_RDWR)) < 0) {
		warn ("open(dev=%s, O_RDWR)", dev);
		return out = false;
	}

	if (! fix_c6state_pkg (dev, fd)) {
		warnx ("fix_c6state_pkg(dev=%s, fd=%d)", dev, fd);
		goto err;
	}

	if (! fix_c6state_core (dev, fd)) {
		warnx ("fix_c6state_core(dev=%s, fd=%d)", dev, fd);
		goto err;
	}

err:
	if (close (fd) != 0) {
		warn ("close(fd=%d)", fd);
		return out = false;
	}

	return out;
}

static bool
fix_c6state_pkg (const char *dev, const int fd)
{
	uintptr_t pkg_msr;
	if (! rdmsr (dev, fd, MSR_PKG, &pkg_msr)) {
		warnx ("rdmsr(dev=%s, fd=%d, msr=%#x)", dev, fd, MSR_PKG);
		return false;
	}

	pkg_msr &= ~BIT(32);

	if (! wrmsr (dev, fd, MSR_PKG, pkg_msr)) {
		warnx ("wrmsr(dev=%s, fd=%d, msr=%#x, val=%#" PRIxPTR ")",
			dev, fd, MSR_PKG, pkg_msr);
		return false;
	}

	return true;
}

static bool
fix_c6state_core (const char *dev, const int fd)
{
	uintptr_t core_msr;
	if (! rdmsr (dev, fd, MSR_CORE, &core_msr)) {
		warnx (
			"rdmsr(dev=%s, fd=%d, msr=%#x)",
			dev, fd, MSR_CORE
		);
		return false;
	}

	core_msr &= ~(BIT(22) | BIT(14) | BIT(6));

	if (! wrmsr (dev, fd, MSR_CORE, core_msr)) {
		warnx (
			"wrmsr(dev=%s, fd=%d, msr=%#x, val=%#" PRIxPTR ")",
			dev, fd, MSR_CORE, core_msr
		);
		return false;
	}

	return true;
}

static bool
rdmsr (const char *dev, const int fd, const uint32_t msr, uintptr_t *out)
{
	int err;
	cpuctl_msr_args_t args = { .msr = msr };
	if ((err = ioctl (fd, CPUCTL_RDMSR, &args)) < 0)
		warn ("ioctl(%s, CPUCTL_RDMSR (%#x))", dev, msr);
	if (out) *out = args.data;
	return err >= 0;
}

static bool
wrmsr (const char *dev, const int fd, const uint32_t msr, const uintptr_t val)
{
	int err;
	cpuctl_msr_args_t args = { .msr = msr, .data = val };
	if ((err = ioctl (fd, CPUCTL_WRMSR, &args)) < 0)
		warn (
			"ioctl(%s, CPUCTL_WRMSR (%#x := %#" PRIxPTR ")",
			dev, msr, val
		);
	return err >= 0;
}
