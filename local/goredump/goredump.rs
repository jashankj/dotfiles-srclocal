/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
 */

//! Drop-in replacement for ``systemd-coredump``.

use std::{
	collections::HashMap,
	path::{PathBuf, Path},
	str::FromStr,
};

use clap::Parser;

use color_eyre::eyre::Result;

use tokio::{
	fs::{File, read_link},
	io::{AsyncReadExt, AsyncWriteExt},
};

// kernel.core_pattern = |/usr/local/libexec/goredump %P %u %g %s %t %c %h
// kernel.core_pattern = |/usr/lib/systemd/systemd-coredump ...
//	%P	PID of dumped process, as seen in the initial PID namespace (since Linux 3.12).
//	%u	Numeric real UID of dumped process.
//	%g	Numeric real GID of dumped process.
//	%s	Number of signal causing dump.
//	%t	Time of dump, expressed as seconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC).
//	%c	Core file size soft resource limit of crashing process (since Linux 2.6.24).
//	%h	Hostname (same as nodename returned by uname(2)).

#[derive(Clone, Debug, Parser, serde::Serialize)]
#[clap(author, version, about, long_about = None)]
struct Args {
	pid:   usize,
	uid:   usize,
	gid:   usize,
	sig:   usize,
	time:  usize,
	rcore: String,
	host:  String
}

#[repr(transparent)]
#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Serialize)]
struct Pid (usize);

#[repr(transparent)]
#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Serialize)]
struct Tid (usize);

/// version of the metadata layout; bump if it changes.
const METADATA_FORMAT: usize = 2022_06_17_002;

#[derive(Debug, serde::Serialize)]
struct Meta {
	v: usize,
	args: Args,
	proc: HashMap<Pid, ProcMeta>,
	task: HashMap<Tid, TaskMeta>,
}

type Idmap = Vec<(usize, usize, usize)>;

// This field list derives from the definition of ``tgid_base_stuff``
// in ``proc/fs/base.c``.
#[derive(Debug, serde::Serialize)]
struct ProcMeta {
//x	task ***
//x	fd ****
//x	map_files
//x	fdinfo
//x	ns
//x	net				// if NET
	environ: Vec<(String, String)>,
//x	auxv
//x	status (derived from stat)
	personality: String,
	limits: Vec<String>,		// XXX
	sched: Vec<String>,		// if SCHED_DEBUG
//x	autogroup			// if SCHED_AUTOGROUP
	timens_offsets: Vec<String>,	// if TIME_NS
//x	comm
	syscall: String,		// if HAVE_ARCH_TRACEHOOK
	cmdline: Vec<String>,
	stat: String,
	statm: String,
	maps: Vec<String>,		// XXX
//	numa_maps: String,		// if NUMA
//x	mem XXX
	cwd: PathBuf,
	root: PathBuf,
	exe: PathBuf,
	mounts: Vec<String>,		// XXX
	mountinfo: Vec<String>,		// XXX
	mountstats: Vec<String>,	// XXX
//x	clear_refs			// if PROC_PAGE_MONITOR
	smaps: Vec<String>,		// if PROC_PAGE_MONITOR
//x	smaps_rollup (derived)		// if PROC_PAGE_MONITOR
//	pagemap: Vec<u8>,		// if PROC_PAGE_MONITOR
	// (see admin-guide/mm/pagemap.rst)
//x	attr				// if SECURITY
	wchan: String,			// if KALLSYMS
//x	stack				// if STACKTRACE
	schedstat: String,		// if SCHED_INFO
//x	latency:			// if LATENCYTOP
	cpuset: String,			// if PROC_PID_CPUSET
	cgroup: String,			// if CGROUPS
//	cpu_resctrl_groups: String,	// if PROC_CPU_RESCTRL
	oom_score: isize,
	oom_adj: isize,
	oom_score_adj: isize,
	loginuid: usize,		// if AUDIT
	sessionid: String,		// if AUDIT
	// [FAULT_INJECTION]
//	coredump_filter: String,	// if ELF_CORE
	io: ProcIoMeta,			// if TASK_IO_ACCOUNTING
	uid_map: Idmap,			// if USER_NS
	gid_map: Idmap,			// if USER_NS
	projid_map: Idmap,		// if USER_NS
	setgroups: String,		// if USER_NS
	timers: Vec<String>,		// if CHECKPOINT_RESTORE && POSIX_TIMERS
//x	timerslack_ns
	patch_state: isize,		// if LIVEPATCH
//	stack_depth			// if STACKLEAK_METRICS
//x	arch_status			// if PROC_PID_ARCH_STATUS
//	seccomp_cache			// if SECCOMP_CACHE_DEBUG
}

#[derive(Debug, serde::Serialize)]
struct ProcIoMeta {
	rchar: usize,
	wchar: usize,
	syscr: usize,
	syscw: usize,
	read_bytes: usize,
	write_bytes: usize,
	cancelled_write_bytes: usize,
}

#[derive(Debug, serde::Serialize)]
struct TaskMeta {}

// Default coredump and metadata output directory, if root.
const GOREDIR: &'static str = "/var/lib/goredump";
// If not run as root, uses XDG_DATA_HOME relative to APPID.
const APPID:   &'static str = "au.com.rulingia.goredump";

////////////////////////////////////////////////////////////////////////

// #[tokio::main]
#[tokio::main(flavor = "multi_thread")]
// #[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()>
{
	color_eyre::install()?;
	let args = Args::parse();

	println!("{:?}", &args);
	let pid = Pid(args.pid);

	let out_name      = format!("{}.{}.{}", args.time, args.host, args.pid);
	let out_meta      = format!("{}.meta", out_name);
	let out_core      = format!("{}.core.zstd", out_name);
	let (out_meta_path, out_core_path) =
		if nix::unistd::getuid().is_root() {
			(
				[GOREDIR, &out_meta].iter().collect(),
				[GOREDIR, &out_core].iter().collect(),
			)
		} else {
			let xdg_dirs = xdg::BaseDirectories::with_prefix(APPID)?;
			(
				xdg_dirs.place_data_file(&out_meta)?,
				xdg_dirs.place_data_file(&out_core)?
			)
		};

	match tokio::try_join!(
		metadata(&args, pid, out_meta_path),
		coredump(&args, pid, out_core_path),
	) {
		Ok(((), ())) => Ok(()),
		Err(e)       => Err(e)
	}
}

////////////////////////////////////////////////////////////////////////

//#[tracing::instrument]
async fn metadata<P> (args: &Args, pid: Pid, out_path: P) -> Result<()>
where P: AsRef<Path> + std::fmt::Debug
{
	let meta = load_metadata(args, pid).await?;
	write_metadata(&meta, &out_path).await?;
	fix_perms_on(&args, &out_path)?;
	println!("{}", out_path.as_ref().display());
	Ok(())
}

//#[tracing::instrument]
async fn load_metadata (args: &Args, pid: Pid) -> Result<Meta>
{
	Ok(Meta {
		v:    METADATA_FORMAT,
		args: args.clone(),
		proc: HashMap::from([(pid, read_procfs_of(pid).await?)]),
		task: HashMap::new(),
	})
}

//#[tracing::instrument]
async fn write_metadata<P> (meta: &Meta, out_path: P) -> Result<()>
where P: AsRef<Path> + std::fmt::Debug
{
	let pretty = ron::ser::PrettyConfig::new()
		.indentor("\t".to_owned());
	let mut out_file = open_write(out_path).await?;
	let outstr = ron::ser::to_string_pretty(&meta, pretty)?;
	out_file.write_all(outstr.as_bytes()).await?;
	Ok(())
}

////////////////////////////////////////////////////////////////////////

//#[tracing::instrument]
async fn coredump<P> (args: &Args, _pid: Pid, out_path: P) -> Result<()>
where P: AsRef<Path> + std::fmt::Debug
{
	use async_compression::tokio::write::ZstdEncoder;
	let mut stdin    = tokio::io::stdin();
	let mut out_file = ZstdEncoder::new(open_write(&out_path).await?);
	let _len = tokio::io::copy(&mut stdin, &mut out_file).await?;
	fix_perms_on(&args, &out_path)?;
	println!("{}", out_path.as_ref().display());
	Ok(())
}

////////////////////////////////////////////////////////////////////////

//#[tracing::instrument]
async fn read_procfs_of (pid: Pid) -> Result<ProcMeta>
{
	Ok(ProcMeta {
		environ:	tokio::spawn(read_environ(pid)).await??,
		personality:	tokio::spawn(read_personality(pid)).await??,
		limits:		tokio::spawn(read_limits(pid)).await??,
		sched:		tokio::spawn(read_sched(pid)).await??,
		timens_offsets:	tokio::spawn(read_timens_offsets(pid)).await??,
		syscall:	tokio::spawn(read_syscall(pid)).await??,
		cmdline:	tokio::spawn(read_cmdline(pid)).await??,
		stat:		tokio::spawn(read_stat(pid)).await??,
		statm:		tokio::spawn(read_statm(pid)).await??,
		maps:		tokio::spawn(read_maps(pid)).await??,
		cwd:		tokio::spawn(read_cwd(pid)).await??,
		root:		tokio::spawn(read_root(pid)).await??,
		exe:		tokio::spawn(read_exe(pid)).await??,
		mounts:		tokio::spawn(read_mounts(pid)).await??,
		mountinfo:	tokio::spawn(read_mountinfo(pid)).await??,
		mountstats:	tokio::spawn(read_mountstats(pid)).await??,
		smaps:		tokio::spawn(read_smaps(pid)).await??,
		wchan:		tokio::spawn(read_wchan(pid)).await??,
		schedstat:	tokio::spawn(read_schedstat(pid)).await??,
		cpuset:		tokio::spawn(read_cpuset(pid)).await??,
		cgroup:		tokio::spawn(read_cgroup(pid)).await??,
		oom_score:	tokio::spawn(read_oom_score(pid)).await??,
		oom_adj:	tokio::spawn(read_oom_adj(pid)).await??,
		oom_score_adj:	tokio::spawn(read_oom_score_adj(pid)).await??,
		loginuid:	tokio::spawn(read_loginuid(pid)).await??,
		sessionid:	tokio::spawn(read_sessionid(pid)).await??,
		io:		tokio::spawn(read_io(pid)).await??,
		uid_map:	tokio::spawn(read_uid_map(pid)).await??,
		gid_map:	tokio::spawn(read_gid_map(pid)).await??,
		projid_map:	tokio::spawn(read_projid_map(pid)).await??,
		setgroups:	tokio::spawn(read_setgroups(pid)).await??,
		timers:	        tokio::spawn(read_timers(pid)).await??,
		patch_state:	tokio::spawn(read_patch_state(pid)).await??,
	})
}

//#[tracing::instrument]
async fn read_cgroup (pid: Pid) -> Result<String>
{
	read_as_string(pid, "cgroup").await
}

//#[tracing::instrument]
async fn read_cmdline (pid: Pid) -> Result<Vec<String>>
{
	let     xs = read_as_vec_null_vec_u8(pid, "cmdline").await?;
	let mut ys = Vec::with_capacity(xs.capacity());
	for x in xs { ys.push(String::from_utf8(x)?); }
	Ok(ys)
}

//#[tracing::instrument]
async fn read_cpuset (pid: Pid) -> Result<String>
{
	read_as_string(pid, "cpuset").await
}

//#[tracing::instrument]
async fn read_cwd (pid: Pid) -> Result<PathBuf>
{
	read_link_target(pid, "cwd").await
}

//#[tracing::instrument]
async fn read_environ (pid: Pid) -> Result<Vec<(String, String)>>
{
	Ok(read_as_vec_null_vec_u8(pid, "environ").await?
		.iter()
		.filter(|xs| !xs.is_empty())
		.map(|xs| {
			let mut it = xs.splitn(2, |&x| x == b'=');
			let f =
				|it: &mut std::slice::SplitN<_, _>|
			String::from_utf8(it.next().unwrap().to_vec()).unwrap();
			let k = f(&mut it); let v = f(&mut it);
			(k, v)
		})
		.collect())
}

//#[tracing::instrument]
async fn read_exe (pid: Pid) -> Result<PathBuf>
{
	read_link_target(pid, "exe").await
}

//#[tracing::instrument]
async fn read_gid_map (pid: Pid) -> Result<Idmap>
{
	read_idmap(pid, "gid_map").await
}

//#[tracing::instrument]
async fn read_io (pid: Pid) -> Result<ProcIoMeta>
{
	let mut rchar                 = None;
	let mut wchar                 = None;
	let mut syscr                 = None;
	let mut syscw                 = None;
	let mut read_bytes            = None;
	let mut write_bytes           = None;
	let mut cancelled_write_bytes = None;
	for line in read_as_lines(pid, "io").await? {
		if let Some(x) = line.strip_prefix("rchar: ") {
			rchar = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("wchar: ") {
			wchar = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("syscr: ") {
			syscr = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("syscw: ") {
			syscw = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("read_bytes: ") {
			read_bytes = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("write_bytes: ") {
			write_bytes = Some(usize::from_str(x)?);
		} else if let Some(x) = line.strip_prefix("cancelled_write_bytes: ") {
			cancelled_write_bytes = Some(usize::from_str(x)?);
		}
	}

	Ok(ProcIoMeta {
		rchar: rchar.unwrap(),
		wchar: wchar.unwrap(),
		syscr: syscr.unwrap(),
		syscw: syscw.unwrap(),
		read_bytes: read_bytes.unwrap(),
		write_bytes: write_bytes.unwrap(),
		cancelled_write_bytes: cancelled_write_bytes.unwrap()
	})
}

//#[tracing::instrument]
async fn read_limits (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "limits").await
}

//#[tracing::instrument]
async fn read_loginuid (pid: Pid) -> Result<usize>
{
	read_as_uint(pid, "loginuid").await
}

//#[tracing::instrument]
async fn read_maps (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "maps").await
}

//#[tracing::instrument]
async fn read_mountinfo (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "mountinfo").await
}

//#[tracing::instrument]
async fn read_mounts (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "mounts").await
}

//#[tracing::instrument]
async fn read_mountstats (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "mountstats").await
}

//#[tracing::instrument]
async fn read_oom_adj (pid: Pid) -> Result<isize>
{
	read_as_sint(pid, "oom_adj").await
}

//#[tracing::instrument]
async fn read_oom_score (pid: Pid) -> Result<isize>
{
	read_as_sint(pid, "oom_score").await
}

//#[tracing::instrument]
async fn read_oom_score_adj (pid: Pid) -> Result<isize>
{
	read_as_sint(pid, "oom_score_adj").await
}

//#[tracing::instrument]
async fn read_patch_state (pid: Pid) -> Result<isize>
{
	read_as_sint(pid, "patch_state").await
}

//#[tracing::instrument]
async fn read_personality (pid: Pid) -> Result<String>
{
	read_as_string(pid, "personality").await
}

//#[tracing::instrument]
async fn read_projid_map (pid: Pid) -> Result<Idmap>
{
	read_idmap(pid, "projid_map").await
}

//#[tracing::instrument]
async fn read_root (pid: Pid) -> Result<PathBuf>
{
	read_link_target(pid, "root").await
}

//#[tracing::instrument]
async fn read_sched (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "sched").await
}

//#[tracing::instrument]
async fn read_schedstat (pid: Pid) -> Result<String>
{
	read_as_string(pid, "schedstat").await
}

//#[tracing::instrument]
async fn read_sessionid (pid: Pid) -> Result<String>
{
	read_as_string(pid, "sessionid").await
}

//#[tracing::instrument]
async fn read_setgroups (pid: Pid) -> Result<String>
{
	read_as_string(pid, "setgroups").await
}

//#[tracing::instrument]
async fn read_smaps (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "smaps").await
}

//#[tracing::instrument]
async fn read_stat (pid: Pid) -> Result<String>
{
	read_as_string(pid, "stat").await
}

//#[tracing::instrument]
async fn read_statm (pid: Pid) -> Result<String>
{
	read_as_string(pid, "statm").await
}

//#[tracing::instrument]
async fn read_syscall (pid: Pid) -> Result<String>
{
	read_as_string(pid, "syscall").await
}

//#[tracing::instrument]
async fn read_timens_offsets (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "timens_offsets").await
}

//#[tracing::instrument]
async fn read_timers (pid: Pid) -> Result<Vec<String>>
{
	read_as_lines(pid, "timers").await
}

//#[tracing::instrument]
async fn read_uid_map (pid: Pid) -> Result<Idmap>
{
	read_idmap(pid, "uid_map").await
}

//#[tracing::instrument]
async fn read_wchan (pid: Pid) -> Result<String>
{
	read_as_string(pid, "wchan").await
}

////////////////////////////////////////////////////////////////////////

/// Read an entire file as a bytestring.
//#[tracing::instrument]
async fn read_as_vec_u8 (pid: Pid, obj: &'static str) -> Result<Vec<u8>>
{
	let path     = proc_path_of(pid, obj);
	let mut file = open_read(path).await?;
	let mut buf  = Vec::new();
	file.read_to_end(&mut buf).await?;
	Ok(buf)
}

/// Read an entire file as nul-separated bytestrings.
//#[tracing::instrument]
async fn read_as_vec_null_vec_u8 (pid: Pid, obj: &'static str) -> Result<Vec<Vec<u8>>>
{
	Ok(read_as_vec_u8(pid, obj).await?
		.split(|&x| x == 0)
		.map(ToOwned::to_owned)
		.collect())
}

/// Read an entire file as a string.
//#[tracing::instrument]
async fn read_as_string (pid: Pid, obj: &'static str) -> Result<String>
{
	Ok(String::from_utf8(
		read_as_vec_u8(pid, obj).await?
	)?)
}

/// Read an entire file containing one unsigned integral number.
//#[tracing::instrument]
async fn read_as_uint (pid: Pid, obj: &'static str) -> Result<usize>
{
	Ok(usize::from_str(
		&read_as_string(pid, obj).await?.trim()
	)?)
}

/// Read an entire file containing one signed integral number.
//#[tracing::instrument]
async fn read_as_sint (pid: Pid, obj: &'static str) -> Result<isize>
{
	Ok(isize::from_str(
		&read_as_string(pid, obj).await?.trim()
	)?)
}

/// Read an entire file containing lines of text.
//#[tracing::instrument]
async fn read_as_lines (pid: Pid, obj: &'static str) -> Result<Vec<String>>
{
	Ok(read_as_string(pid, obj).await?
		.lines()
		.map(ToOwned::to_owned)
		.collect())
}

/// Read a file containing an id-map table (gid, uid, projid).
//#[tracing::instrument]
async fn read_idmap (pid: Pid, obj: &'static str) -> Result<Idmap>
{
	let mut out = vec![];
	for l in read_as_lines(pid, obj).await? {
		let xs: Vec<usize> = l.split_whitespace()
			.map(usize::from_str)
			.map(Result::unwrap)
			.collect();
		match xs[..] {
			[x, y, z] => { out.push((x, y, z)) },
			_ => panic!("map file ill-formed: {:?}", l)
		}
	}
	Ok(out)
}

//#[tracing::instrument]
async fn read_link_target (pid: Pid, obj: &'static str) -> Result<PathBuf>
{
	Ok(read_link(proc_path_of(pid, obj)).await?)
}

//#[tracing::instrument]
fn proc_path_of (pid: Pid, obj: &'static str) -> PathBuf
{
	PathBuf::from(format!("/proc/{}/{}", pid.0, obj))
}

////////////////////////////////////////////////////////////////////////

//#[tracing::instrument]
async fn open_read<P> (path: P) -> Result<File>
where P: AsRef<Path> + std::fmt::Debug
{
	Ok(tokio::fs::OpenOptions::new()
		.read(true)
		.open(path).await?)
}

//#[tracing::instrument]
async fn open_write<P> (path: P) -> Result<File>
where P: AsRef<Path> + std::fmt::Debug
{
	Ok(tokio::fs::OpenOptions::new()
		.mode(0o600)
		.create(true)
		.write(true)
		.open(path).await?)
}

fn fix_perms_on<P> (args: &Args, path: P) -> Result<()>
where P: AsRef<Path> + std::fmt::Debug
{
	use nix::unistd::{chown, Uid, Gid};
	let uid = Uid::from_raw(args.uid as u32);
	let gid = Gid::from_raw(args.gid as u32);
	Ok(chown(path.as_ref(), Some(uid), Some(gid))?)
}

////////////////////////////////////////////////////////////////////////
