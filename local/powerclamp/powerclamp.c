/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2016 Jashank Jeremy <jashank at rulingia.com.au>
 */

/**
 * @brief	userland command to set intel powerclamp
 * @author	Jashank Jeremy &lt;jashank at rulingia.com.au&gt;
 * @date	2016-12-31
 */

#include <sys/cdefs.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <sysexits.h>
#include <unistd.h>

/* __RCSID("$Id: powerclamp.c,v 1.6 2016/12/31 01:54:07 jashank Exp $"); */

#define POWERCLAMP_PATH_jaenelle "/sys/class/thermal/cooling_device4/cur_state"
#define POWERCLAMP_PATH_yarn     "/sys/class/thermal/cooling_device8/cur_state"

#ifndef POWERCLAMP_PATH
#define POWERCLAMP_PATH POWERCLAMP_PATH_yarn
#endif

#define USAGE_MSG "usage: powerclamp <0..50>"

int
main (int argc, char *argv[]) {
	if (geteuid () != 0)
		errx (EX_USAGE, "should be run as root...");

	if (argc != 2)
		errx (EX_USAGE, USAGE_MSG);

	long int percent = strtol (argv[1], NULL, 10);
	if (! (0 <= percent && percent <= 50))
		errx (EX_USAGE, USAGE_MSG);

	FILE *fp = fopen (POWERCLAMP_PATH, "w");
	if (fp == NULL)
		err (EX_OSERR, "couldn't open " POWERCLAMP_PATH);

	int ret = fprintf (fp, "%li\n", percent);
	if (ret <= 0)
		err (EX_OSERR, "couldn't write to " POWERCLAMP_PATH);

	if (fclose (fp) != 0)
		err (EX_OSERR, "couldn't close " POWERCLAMP_PATH);

	return EXIT_SUCCESS;
}
