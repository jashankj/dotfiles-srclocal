/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unused_imports)]

use color_eyre::eyre::{Result, eyre};

use gtk4 as gtk;
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Button};

fn main () -> Result<()>
{
	color_eyre::install()?;

	let application = Application::builder()
		.application_id("au.com.rulingia.sudokio")
		.build();

	application.connect_activate(|app| {
		let window = ApplicationWindow::builder()
			.application(app)
			.title("Sudokio")
			.default_width(350)
			.default_height(70)
			.build();

		window.show();
	});

	application.run();
	Ok(())
}


/*

concept ---

HashSet<(u8, u8, Cell)>



*/
