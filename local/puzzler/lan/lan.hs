--
-- SPDX-License-Identifier: Apache-2.0
-- Copyright 2018-22 Jashank Jeremy <jashank at rulingia.com.au>
--

-- | Solver for Countdown.
--
-- @
-- $ lan letters tnettenba
-- $ lan numbers 5 6 25 50 75 100 336
-- $ lan conundrum conundrum
-- @
--
-- 2018-11-18   Jashank Jeremy <jashank at rulingia.com.au>
-- 2019-09-08   Jashank Jeremy <jashank at rulingia.com.au>
--      - use MonadComprehensions for filtering
--      - add commutativity-aware equality for numbers

{-# OPTIONS_GHC -O3 #-}
{-# OPTIONS_GHC -fllvm #-}
{-# OPTIONS_GHC -threaded #-}
{-# OPTIONS_GHC -rtsopts #-}
{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE StandaloneDeriving #-}

#define DICTIONARY_SET

module Main (main) where

import           Control.Monad
import           Data.List
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as Set
import           System.Environment
import           System.IO

main :: IO ()
main = getArgs >>= \case
  ["letters", x]   -> doLetters' x
  ["letters"]      -> doLetters

  ["numbers", a, b, c, d, e, f, t]
                   -> doNumbers' [a, b, c, d, e, f] t
  ["numbers"]      -> doNumbers

  ["conundrum", x] -> doConundrum' x
  ["conundrum"]    -> doConundrum
  ["teaser", x]    -> doConundrum' x
  ["teaser"]       -> doConundrum

  _                -> usage
  where
    usagestr =
      [ "usage: lan (letters|numbers|conundrum|teaser) [...]"
      , "       lan letters WORD"
      , "       lan numbers A B C D E F TARGET"
      , "       lan conundrum WORD"
      , "       lan teaser WORD"
      ]
    usage = hPutStr stderr $ unlines usagestr

------------------------------------------------------------------------

doLetters :: IO ()
doLetters = prompt "Letters: " >>= doLetters'

doNumbers :: IO ()
doNumbers
  =          prompt "Numbers: "
  >>= \xs -> prompt "Target: "
  >>= \t  -> doNumbers' (words xs) t

doConundrum :: IO ()
doConundrum = prompt "Conundrum: " >>= doConundrum'

prompt :: String -> IO String
prompt text = putStr text >> hFlush stdout >> getLine

------------------------------------------------------------------------

doLetters' :: [L] -> IO ()
doLetters' word
  =            loadDict
  >>= \dict -> showResults $ letters dict word
  where
    showResults     = mapM_ (\(l, xs) -> showResult l xs)
    showResult l xs = putStrLn $ (show l) ++ "\t" ++ unwords xs

doNumbers' :: [String] -> String -> IO ()
doNumbers' xs t
  = showResults $ numbers (readNums xs) (read t)
  where
    showResults      = sequence_ . map (uncurry showResult) . nub
    showResult op t_ = putStrLn $ show t_ ++ " == " ++ show op
    readNums         = map read

doConundrum' :: [L] -> IO ()
doConundrum' word
  =            loadDict
  >>= \dict -> showResults $ conundrum dict word
  where
    showResults = putStrLn . unwords

------------------------------------------------------------------------

type L = Char

loadDict :: IO Dict
isWord :: Dict -> [L] -> Bool

wordsFile :: FilePath
wordsFile = "/usr/share/dict/words"

#ifdef DICTIONARY_SET

-- dictionaries using a binary-tree set for SPEEEEEED
-- yields roughly 7-8x performance improvement :O
type Dict = Set [L]
loadDict = readFile wordsFile >>= return . Set.fromList . lines
isWord dict word = word `Set.member` dict

#else

-- dictionaries using just a plain old list of strings
type Dict = [String]
loadDict = readFile wordsFile >>= return . lines
isWord dict word = word `elem` dict

#endif

letters :: Dict -> [L] -> [(Int, [String])]
letters dict str -- @[_a, _b, _c, _d, _e, _f, _g, _h, _i]
  = groupify
  . groupBy (\x y -> lengthOrd x y == EQ)
  . sortBy lengthOrd
  . filter (isWord dict)
  . quicknub . countdownify $ str
  where
    lengthOrd = \x -> \y -> compare (length x) (length y)
    groupify  = map (\xs -> (length $ head xs, xs))
-- letters _ _ = error "bad input"

conundrum :: Dict -> [L] -> [String]
conundrum dict word
  = filter (isWord dict) . quicknub . permutations $ word

------------------------------------------------------------------------

type N = Int
data Ops where
  K :: N -> Ops
  Add :: Ops -> Ops -> Ops
  Sub :: Ops -> Ops -> Ops
  Mul :: Ops -> Ops -> Ops
  Div :: Ops -> Ops -> Ops

-- commutativity-aware equality prunes massive repetition out of the
-- output `doNumbers'` mechanism, with a well-placed call to `nub`
instance Eq Ops where
  (==) (K k1) (K k2) = k1 == k2
  (==) (Add a1 b1) (Add a2 b2) = coeq (a1, b1) (a2, b2)
  (==) (Mul a1 b1) (Mul a2 b2) = coeq (a1, b1) (a2, b2)
  (==) (Sub a1 b1) (Sub a2 b2) = a1 == a2 && b1 == b2
  (==) (Div a1 b1) (Div a2 b2) = a1 == a2 && b1 == b2
  (==) _ _ = False

coeq :: Eq a => (a, a) -> (a, a) -> Bool
coeq (a1, b1) (a2, b2)
  =  (a1 == a2 && b1 == b2)
  || (a1 == b2 && a2 == b1)

instance Show Ops where
  show (K n) = show n
  show (Add a b) = "(" ++ show a ++ " + " ++ show b ++ ")"
  show (Sub a b) = "(" ++ show a ++ " - " ++ show b ++ ")"
  show (Mul a b) = "(" ++ show a ++ " * " ++ show b ++ ")"
  show (Div a b) = "(" ++ show a ++ " / " ++ show b ++ ")"

numbers :: [N] -> N -> [(Ops, N)]
numbers nums target = hits
  where
    hits = [ (x, y) | (x, (Just y)) <- okevals, y `elem` nearTarget ]
    nearTarget = [ (target - targetrange) .. (target + targetrange) ]
    targetrange = 0
    okevals = filter (isJust . snd) evalops
    evalops = map (\x -> (x, eval x)) ops
    ops  = concatMap (insertop) seqs
    seqs = countdownify nums
-- numbers (a,b,c,d,e,f) target = hits
--  nums = [a, b, c, d, e, f]

eval :: Ops -> Maybe N
eval x = do
  res <- eval' x
  when (res < 0) $ fail ""
  return res

eval' :: Ops -> Maybe N
eval' (K n)     = Just n
eval' (Add a b) = pure (+) <*> eval a <*> eval b
eval' (Sub a b) = pure (-) <*> eval a <*> eval b
eval' (Mul a b) = pure (*) <*> eval a <*> eval b
eval' (Div a b) =
  [ a_ `div` b_
  | a_ <- eval a, b_ <- eval b
  , b_ /= 0, b_ /= 1 -- don't divide by 0 or 1
  , a_ `mod` b_ == 0 -- CDN rules say exact divison only
  ]

insertop :: [N] -> [Ops]
insertop []     = []
insertop (x:[]) = [K x]
insertop (x_:xs) =
  concatMap (\xs' ->
               [ Add x xs', Add xs' x
               , Sub x xs', Sub xs' x
               , Mul x xs', Mul xs' x
               , Div x xs', Div xs' x
               ]) $ insertop xs
  where x = K x_

----------------------------------------------------------------------------------

countdownify :: [a] -> [[a]]
countdownify = concatMap (permutations) . subsequences

quicknub :: Ord a => [a] -> [a]
quicknub = Set.toList . Set.fromList
