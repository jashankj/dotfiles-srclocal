/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

use nytsolv::solver::*;
use z3::*;
use crate::{Cell, N, GSudoku, Sudoku, SudokuPartial};

pub fn run (data: &SudokuPartial) -> Option<Sudoku>
{
	nytsolv::solver::run_with_z3(|ctx, solv| {
		let (cs, cc, _ct) = setup_types(&ctx);
		let grid = setup_objects(&ctx, &cs);
		setup_axioms(&ctx, &solv, &grid);
		setup_model(&solv, &cc, data, &grid);

		use SatResult::*;
		match solv.check() {
			SatResult::Unknown => {
				println!("{}", solv.get_reason_unknown().unwrap());
				None
			},
			SatResult::Unsat => {
				println!("{:#?}", solv.get_unsat_core());
				None
			},
			SatResult::Sat => {
	//			println!("sat");
	//			println!("{:#?}", solv.get_model().unwrap());
				Some(read_model(&cc, &grid, &solv.get_model().unwrap()))
			},
		}
	})
}

#[test]
fn try_prover ()
{
	let (input, expect) = crate::test_vector_0();
	let actual = run(&input).unwrap();
	println!("{}", input.format_whole(&expect));
	println!("{}", input.format_whole(&actual));
	assert_eq!(&actual, &expect);
}

////////////////////////////////////////////////////////////////////////

fn setup_types (ctx: &Context) ->
	(Sort<'_>, Vec<FuncDecl<'_>>, Vec<FuncDecl<'_>>)
{
//	$z3->send($_."\n") foreach (
//		"(declare-datatype Cell ((n1) (n2) (n3) (n4) (n5) (n6) (n7) (n8) (n9)))",
//	);

	let syms: Vec<_> = (1..=N)
		.map(|x| format!("n{}", x))
		.map(Symbol::from)
		.collect();
	Sort::enumeration(ctx, "Cell".into(), &syms)
}

////////////////////////////////////////////////////////////////////////

fn setup_objects<'ctx> (ctx: &'ctx Context, cs: &Sort<'ctx>)
	-> GSudoku<(Symbol, ast::Datatype<'ctx>)>
{
//	foreach my $i (0..8) {
//		foreach my $j (0..8) {
//			$z3->send(
//				sprintf("(declare-const \%s Cell)\n", cell_ref($i, $j))
//			)
//		}
//	}

	GSudoku(r((0..N).map(|i| setup_row(ctx, cs, i))))
}

fn setup_row<'ctx> (
	ctx: &'ctx Context,
	cs:  &Sort<'ctx>,
	i:   usize,
) -> [(Symbol, ast::Datatype<'ctx>); N]
{
	r((0..N).map(|j| setup_one(ctx, cs, i, j)))
}

fn setup_one<'ctx> (
	ctx: &'ctx Context,
	cs:  &Sort<'ctx>,
	i:   usize,
	j:   usize
) -> (Symbol, ast::Datatype<'ctx>)
{
	let x: String = Sudoku::cell_ref(i, j).into_iter().collect();
	let x = Symbol::from(x);
	(x.clone(), ast::Datatype::new_const(ctx, x, cs))
}

////////////////////////////////////////////////////////////////////////

fn setup_axioms<'ctx> (
	ctx:  &'ctx Context,
	solv: &Solver<'ctx>,
	grid: &GSudoku<(Symbol, ast::Datatype<'ctx>)>
)
{
	setup_axioms_row    (ctx, solv, grid);
	setup_axioms_column (ctx, solv, grid);
	setup_axioms_block  (ctx, solv, grid);
}

fn setup_axioms_row<'ctx> (
	ctx:  &'ctx Context,
	solv: &Solver<'ctx>,
	grid: &GSudoku<(Symbol, ast::Datatype<'ctx>)>
)
{
	let GSudoku(grid) = grid;

//	# Distinct rows:
//	foreach my $i (0..8) {
//		assert_distinct($z3, map { cell_ref $i, $_ } (0..8));
//	}
	for i in 0..N {
		let row: [&ast::Datatype; N] =
			r((0..N).map(|j| &grid[i][j].1));
		println!("{:?}", &row);
		assert_distinct(ctx, solv, &row);
	}
}

fn setup_axioms_column<'ctx> (
	ctx:  &'ctx Context,
	solv: &Solver<'ctx>,
	grid: &GSudoku<(Symbol, ast::Datatype<'ctx>)>
)
{
	let GSudoku(grid) = grid;

//	# Distinct columns:
//	foreach my $j (0..8) {
//		assert_distinct($z3, map { cell_ref $_, $j } (0..8));
//	}
	for j in 0..N {
		let col: [&ast::Datatype; N] =
			r((0..N).map(|i| &grid[i][j].1));
		println!("{:?}", &col);
		assert_distinct(ctx, solv, &col);
	}
}

fn setup_axioms_block<'ctx> (
	ctx:  &'ctx Context,
	solv: &Solver<'ctx>,
	grid: &GSudoku<(Symbol, ast::Datatype<'ctx>)>
)
{
	let GSudoku(grid) = grid;

//	# Distinct blocks:
//	foreach my $n (0..2) {
//		foreach my $m (0..2) {
//			assert_distinct(
//				$z3,
//				map {
//					cell_ref(
//						($n * 3) + ($_ / 3),
//						($m * 3) + ($_ % 3)
//					)
//				} (0..8)
//			)
//		}
//	}

	const M: usize = 3;

	fn block_ref (n: usize, m: usize, x: usize) -> (usize, usize)
	{
		((n * M) + (x / M), (m * M) + (x % M))
	}

	for n in 0..M {
		for m in 0..M {
			let block: [&ast::Datatype; N] =
				r(
					(0..N)
						.map(|x| block_ref(n, m, x))
						.map(|(i, j)| &grid[i][j].1)
				);
			println!("{:?}", &block);
			assert_distinct(ctx, solv, &block);
		}
	}
}

fn row_map<T, F, U> (xs: [T; N], f: F) -> [U; N]
where F: Fn(T, usize) -> U, U: std::fmt::Debug
{
	r(
		xs.into_iter().enumerate()
			.map(|(n, x)| f(x, n))
	)
}

fn assert_distinct<'ctx> (ctx: &'ctx Context, solv: &Solver<'ctx>, xs: &[&ast::Datatype<'ctx>])
{
//	$z3->send('(assert (distinct '.join(' ', @_).'))'."\n");
	solv.assert(&ast::Ast::distinct(ctx, xs))
}


fn r<T: std::fmt::Debug, U: Iterator<Item = T>> (xs: U) -> [T; N]
{
	xs.collect::<Vec<_>>().try_into().unwrap()
}

////////////////////////////////////////////////////////////////////////

fn setup_model<'ctx> (
	solv: &Solver<'ctx>,
	cc:   &[FuncDecl<'ctx>],
	data: &SudokuPartial,
	grid: &GSudoku<(Symbol, ast::Datatype<'ctx>)>
)
{
	use z3::ast::Ast;

//	foreach my $i (0..8) {
//		foreach my $j (0..8) {
//			my $x = $I->[$i]->[$j];
//			next unless is_digit($x);
//			$z3->send(
//				sprintf(
//					"(assert (= c\%d\%d n\%d))\n",
//					$i, $j, $x
//				)
//			);
//		}
//	}

	let GSudoku(data) = data;
	let GSudoku(grid) = grid;

	for i in 0..N {
		for j in 0..N {
			if let Some(x) = data[i][j] {
				let n = u8::from(x) as usize;
				let n_x = cc[n-1].apply(&[]).as_datatype().unwrap();
				let c_ij = &grid[i][j].1;
				solv.assert(&c_ij._eq(&n_x));
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////

fn convert_value<'ctx> (
	cc: &[FuncDecl<'ctx>],
	it: ast::Datatype<'ctx>
) -> Cell
{
	cc.iter().enumerate()
		.find_map(|(i, x)| {
			match x.apply(&[]).as_datatype().unwrap() == it {
				true  => Some(Cell::from((i + 1) as u8)),
				false => None,
			}
		}).unwrap()
}

fn read_model<'ctx> (
	cc:    &[FuncDecl<'ctx>],
	grid:  &GSudoku<(Symbol, ast::Datatype<'ctx>)>,
	model: &Model<'ctx>
) -> Sudoku
{
	let GSudoku(grid) = grid;
	GSudoku(
		r((0..N).map(|i| {
			r((0..N).map(|j| {
				let x = model.eval(&grid[i][j].1, true).unwrap();
				convert_value(cc, x)
			}))
		}))
	)
}

////////////////////////////////////////////////////////////////////////

#[cfg(test)]
pub fn test_vector_0() -> (SudokuPartial, Sudoku)
{
	use std::option::Option::None as X;
	use crate::Cell::*;
	let n1 = Some(N1); let n2 = Some(N2); let n3 = Some(N3);
	let n4 = Some(N4); let n5 = Some(N5); let n6 = Some(N6);
	let n7 = Some(N7); let n8 = Some(N8); let n9 = Some(N9);

	let s = GSudoku([
		[ X,  X,  X,   X, n1,  X,   X,  X,  X],
		[ X, n3, n4,  n2,  X,  X,   X,  X,  X],
		[ X,  X,  X,  n7, n5,  X,   X,  n6, X],

		[n5, n1,  X,   X,  X, n8,   X, n3,  X],
		[ X, n6,  X,   X,  X,  X,  n7, n1,  X],
		[ X,  X, n9,   X,  X,  X,   X,  X,  X],

		[n9,  X,  X,  n4,  X, n6,  n3,  X,  X],
		[ X,  X, n1,   X, n3,  X,   X,  X,  X],
		[ X, n2,  X,   X,  X,  X,  n9, n5,  X],
	]);

	let t = GSudoku([
		[N6, N7, N5,  N8, N1, N3,  N4, N9, N2],
		[N1, N3, N4,  N2, N6, N9,  N5, N8, N7],
		[N8, N9, N2,  N7, N5, N4,  N1, N6, N3],

		[N5, N1, N7,  N6, N4, N8,  N2, N3, N9],
		[N4, N6, N3,  N5, N9, N2,  N7, N1, N8],
		[N2, N8, N9,  N3, N7, N1,  N6, N4, N5],

		[N9, N5, N8,  N4, N2, N6,  N3, N7, N1],
		[N7, N4, N1,  N9, N3, N5,  N8, N2, N6],
		[N3, N2, N6,  N1, N8, N7,  N9, N5, N4],
	]);

	(s, t)
}

#[test]
fn z3_hallucination()
{
	use std::option::Option::None as X;
	let s: SudokuPartial = GSudoku([[X; 9]; 9]);
	let actual = run(&s).unwrap();
	println!("{}", s.format_whole(&actual));

//	use Cell::*;
//	let expect = GSudoku([
//		[N8, N6, N5, N2, N4, N9, N1, N3, N7],
//		[N9, N1, N3, N7, N5, N6, N4, N8, N2],
//		[N4, N2, N7, N3, N1, N8, N6, N9, N5],
//		[N3, N5, N2, N9, N7, N4, N8, N6, N1],
//		[N7, N4, N1, N6, N8, N2, N9, N5, N3],
//		[N6, N8, N9, N5, N3, N1, N2, N7, N4],
//		[N1, N7, N8, N4, N6, N3, N5, N2, N9],
//		[N5, N9, N4, N8, N2, N7, N3, N1, N6],
//		[N2, N3, N6, N1, N9, N5, N4, N7, N8]
//	]);
//
//	assert_eq!(&actual, &expect);
}

/*
########################################################################

my $i = read_sudoku_data;
my $o = run $i;
print_pretty_sudoku $o, $i;

# 8 6 5 2 4 9 1 3 7 9 1 3 7 5 6 4 8 2 4 2 7 3 1 8 6 9 5 3 5 2 9 7 4 8 6 1 7 4 1 6 8 2 9 5 3 6 8 9 5 3 1 2 7 4 1 7 8 4 6 3 5 2 9 5 9 4 8 2 7 3 1 6 2 3 6 1 9 5 4 7 8
# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


__DATA__;


;;	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;	;;   ⁰ ¹ ²   ³ ⁴ ⁵   ⁶ ⁷ ⁸
;;	;; ⁰ . . . | . 1 . | . . .
;;	;; ¹ . 3 4 | 2 . . | . . .
;;	;; ² . . . | 7 5 . | . 6 .
;;	;;   ------+-------+------
;;	;; ³ 5 1 . | . . 8 | . 3 .
;;	;; ⁴ . 6 . | . . . | 7 1 .
;;	;; ⁵ . . 9 | . . . | . . .
;;	;;   ------+-------+------
;;	;; ⁶ 9 . . | 4 . 6 | 3 . .
;;	;; ⁷ . . 1 | . 3 . | . . .
;;	;; ⁸ . 2 . | . . . | 9 5 .
;;	(assert
;;	 (and
;;	  (= c04 n1)
;;	  (= c11 n3) (= c12 n4) (= c13 n2)
;;	  (= c23 n7) (= c24 n5) (= c27 n6)
;;	  (= c30 n5) (= c31 n1) (= c35 n8) (= c37 n3)
;;	  (= c41 n6) (= c46 n7) (= c47 n1)
;;	  (= c52 n9)
;;	  (= c60 n9) (= c63 n4) (= c65 n6) (= c66 n3)
;;	  (= c72 n1) (= c74 n3)
;;	  (= c81 n2) (= c86 n9) (= c87 n5)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

. . . . 1 . . . .
. 3 4 2 . . . . .
. . . 7 5 . . 6 .
5 1 . . . 8 . 3 .
. 6 . . . . 7 1 .
. . 9 . . . . . .
9 . . 4 . 6 3 . .
. . 1 . 3 . . . .
. 2 . . . . 9 5 .


*/
