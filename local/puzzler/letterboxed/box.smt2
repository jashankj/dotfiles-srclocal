;; 	      P   E   R
;; 	  ┌───o───o───o───┐
;; 	  │               │
;; 	A o               o N
;; 	  │               │
;; 	O o               o H
;; 	  │               │
;; 	M o               o U
;; 	  │               │
;; 	  └───o───o───o───┘
;; 	      G   Y   T
;;
;; t IN { P, E, R }  &&  u IN { N, H, U, G, Y, T, A, O, M }  &&  bigram t u

(set-option :produce-assignments true)
(set-option :produce-models      true)
(set-option :produce-proofs      true)
(set-option :produce-unsat-cores true)

(declare-datatype
 Char
 ((A) (B) (C) (D) (E) (F) (G) (H) (I)
  (J) (K) (L) (M) (N) (O) (P) (Q) (R)
  (S) (T) (U) (V) (W) (X) (Y) (Z)))


(declare-fun adjacent (Char Char)      Bool)

(assert (adjacent P P))
(assert (adjacent P E))
(assert (adjacent P R))
(assert (adjacent E P))
(assert (adjacent E E))
(assert (adjacent E R))
(assert (adjacent R P))
(assert (adjacent R E))
(assert (adjacent R R))
(assert (adjacent N N))
(assert (adjacent N H))
(assert (adjacent N U))
(assert (adjacent H N))
(assert (adjacent H H))
(assert (adjacent H U))
(assert (adjacent U N))
(assert (adjacent U H))
(assert (adjacent U U))
(assert (adjacent G G))
(assert (adjacent G Y))
(assert (adjacent G T))
(assert (adjacent Y G))
(assert (adjacent Y Y))
(assert (adjacent Y T))
(assert (adjacent T G))
(assert (adjacent T Y))
(assert (adjacent T T))
(assert (adjacent A A))
(assert (adjacent A O))
(assert (adjacent A M))
(assert (adjacent O A))
(assert (adjacent O O))
(assert (adjacent O M))
(assert (adjacent M A))
(assert (adjacent M O))
(assert (adjacent M M))

(assert (and
	 (not (adjacent P N)) (not (adjacent P H)) (not (adjacent P U))
	 (not (adjacent P G)) (not (adjacent P Y)) (not (adjacent P T))
	 (not (adjacent P A)) (not (adjacent P O)) (not (adjacent P M))
	 (not (adjacent E N)) (not (adjacent E H)) (not (adjacent E U))
	 (not (adjacent E G)) (not (adjacent E Y)) (not (adjacent E T))
	 (not (adjacent E A)) (not (adjacent E O)) (not (adjacent E M))
	 (not (adjacent R N)) (not (adjacent R H)) (not (adjacent R U))
	 (not (adjacent R G)) (not (adjacent R Y)) (not (adjacent R T))
	 (not (adjacent R A)) (not (adjacent R O)) (not (adjacent R M))
	 (not (adjacent N P)) (not (adjacent N E)) (not (adjacent N R))
	 (not (adjacent N G)) (not (adjacent N Y)) (not (adjacent N T))
	 (not (adjacent N A)) (not (adjacent N O)) (not (adjacent N M))
	 (not (adjacent H P)) (not (adjacent H E)) (not (adjacent H R))
	 (not (adjacent H G)) (not (adjacent H Y)) (not (adjacent H T))
	 (not (adjacent H A)) (not (adjacent H O)) (not (adjacent H M))
	 (not (adjacent U P)) (not (adjacent U E)) (not (adjacent U R))
	 (not (adjacent U G)) (not (adjacent U Y)) (not (adjacent U T))
	 (not (adjacent U A)) (not (adjacent U O)) (not (adjacent U M))
	 (not (adjacent G P)) (not (adjacent G E)) (not (adjacent G R))
	 (not (adjacent G N)) (not (adjacent G H)) (not (adjacent G U))
	 (not (adjacent G A)) (not (adjacent G O)) (not (adjacent G M))
	 (not (adjacent Y P)) (not (adjacent Y E)) (not (adjacent Y R))
	 (not (adjacent Y N)) (not (adjacent Y H)) (not (adjacent Y U))
	 (not (adjacent Y A)) (not (adjacent Y O)) (not (adjacent Y M))
	 (not (adjacent T P)) (not (adjacent T E)) (not (adjacent T R))
	 (not (adjacent T N)) (not (adjacent T H)) (not (adjacent T U))
	 (not (adjacent T A)) (not (adjacent T O)) (not (adjacent T M))
	 (not (adjacent A P)) (not (adjacent A E)) (not (adjacent A R))
	 (not (adjacent A N)) (not (adjacent A H)) (not (adjacent A U))
	 (not (adjacent A G)) (not (adjacent A Y)) (not (adjacent A T))
	 (not (adjacent O P)) (not (adjacent O E)) (not (adjacent O R))
	 (not (adjacent O N)) (not (adjacent O H)) (not (adjacent O U))
	 (not (adjacent O G)) (not (adjacent O Y)) (not (adjacent O T))
	 (not (adjacent M P)) (not (adjacent M E)) (not (adjacent M R))
	 (not (adjacent M N)) (not (adjacent M H)) (not (adjacent M U))
	 (not (adjacent M G)) (not (adjacent M Y)) (not (adjacent M T))
	 ))

;;	(declare-fun trigram (Char Char Char) Bool)
;;	(assert
;;	 (forall ((t Char) (u Char) (v Char))
;;		 (= (and (adjacent t u) (adjacent u v))
;;		    (trigram t u v))))

(check-sat)
(get-model)
