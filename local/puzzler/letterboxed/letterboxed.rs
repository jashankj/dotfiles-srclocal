/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unused_imports)]

use color_eyre::eyre::{Result, eyre};

mod solv;

#[tracing::instrument]
fn main () -> Result<()>
{
	color_eyre::install()?;

	solv::run();

	Ok(())
}

/*
	      P   E   R
	  ┌───o───o───o───┐
	  │               │
	A o               o N
	  │               │
	O o               o H
	  │               │
	M o               o U
	  │               │
	  └───o───o───o───┘
	      G   Y   T

t IN { P, E, R }  &&  u IN { N, H, U, G, Y, T, A, O, M }  &&  bigram t u
*/

////////////////////////////////////////////////////////////////////////
