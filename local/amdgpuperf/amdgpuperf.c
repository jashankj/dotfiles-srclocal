/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2021, 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

/**
 * @file	~/src/local/amdgpuperf/amdgpuperf.c
 * @brief	userland command to set AMD GPU power management
 * @author	Jashank Jeremy &lt;jashank at rulingia.com.au&gt;
 * @date	2021-04-07
 */

#include <sys/cdefs.h>

#include <err.h>
#include <search.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#define CARD_PATH "/sys/class/drm/card1/device/power_dpm_force_performance_level"
#define USAGE_MSG "usage: amdgpuperf <level>"

/// drivers/gpu/drm/amd/pm/amdgpu_pm.c documents:
static const size_t n_perf_modes = 8;
static const char *perf_modes[/* n_perf_modes */] = {
	"auto",
	"low",
	"high",
	"manual",
	"profile_standard",
	"profile_min_sclk",
	"profile_min_mclk",
	"profile_peak"
};

static inline bool
valid_level_p (const char *str)
{
	for (unsigned i = 0; i < n_perf_modes; i++)
		if (strcmp (str, perf_modes[i]) == 0)
			return true;
	return false;
}

int
main (int argc, char *argv[])
{
	if (geteuid () != 0)
		errx (EX_USAGE, "should be run as root...");

	if (argc != 2)
		errx (EX_USAGE, USAGE_MSG);

	char *level = argv[1];
	if (! valid_level_p (level))
		errx (EX_USAGE, USAGE_MSG);

	FILE *fp = fopen (CARD_PATH, "w");
	if (fp == NULL)
		err (EX_OSERR, "couldn't open " CARD_PATH);

	int ret = fprintf (fp, "%s\n", level);
	if (ret <= 0)
		err (EX_OSERR, "couldn't write to " CARD_PATH);

	if (fclose (fp) != 0)
		err (EX_OSERR, "couldn't close " CARD_PATH);

	return EXIT_SUCCESS;
}
