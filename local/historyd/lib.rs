use chrono::prelude::*;
use serde::{Serialize, Deserialize};
// use serde_cbor;

pub const REMOTE_ADDR: &'static str = "127.0.0.1:6789";

#[derive(Debug, Deserialize, Serialize)]
pub enum Request {
	SessionStart {
		start_time: DateTime<Local>,
		hostname: String,
		username: String,
		shell: String,
		shell_pid: u32
	},

	SessionEnd {
		session_id: usize,
		end_time: DateTime<Local>
	},

	History {
		session_id: usize,
		shell_sequence: Option<usize>,
		shell_level: Option<usize>,
		tty: Option<String>,
		euid: Option<u32>,
		time_started: DateTime<Local>,
		time_finished: DateTime<Local>,
		return_value: u32,
		pipe_status: Vec<u32>,
		working_dir: Option<String>,
		command: String,
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub enum Response {
	Session {
		session_id: usize
	},

	History {
		history_id: usize
	}
}
