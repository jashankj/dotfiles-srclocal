/*-
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 * Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
 *
 * Portions derived from xorg/apps/setxkbmap, which is
 *   Copyright 1996 Silicon Graphics Computer Systems, Inc.
 *   SPDX-License-Identifier: X11
 *
 * Portions derived from xorg/apps/xinput, which is
 *   Copyright 1996-1997 Frederic Lepied, France.
 *   Copyright 2007 Peter Hutterer
 *   Copyright 2009, 2011 Red Hat, Inc.
 *   SPDX-License-Identifier: BSD-0-Clause?
 *
 * Portions derived from xorg/apps/xrandr, which is
 *   Copyright © 2001 Keith Packard, member of The XFree86 Project, Inc.
 *   Copyright © 2002 Hewlett Packard Company, Inc.
 *   Copyright © 2006 Intel Corporation
 *   SPDX-License-Identifier: X11
 */

#![allow(unused_variables)]
#![allow(unused_imports)]

use xcb::{
	x, Connection as XConn,
	randr, xinput, xkb
};
use color_eyre::eyre::{Result, eyre};

////////////////////////////////////////////////////////////////////////

fn main() -> Result<()>
{
	color_eyre::install()?;

	let (x, screen_num) = xcb::Connection::connect_with_extensions(
		None,
		&[
			xcb::Extension::Input,
			xcb::Extension::Xkb,
			xcb::Extension::RandR,
		],
		&[]
	)?;
	let setup  = x.get_setup();
	let screen = setup.roots().nth(screen_num as usize)
		.ok_or_else(|| eyre!("no {}'th screen?", screen_num))?;
	let root   = screen.root();

	// get_screen:
	let (minw, minh, maxw, maxh) = {
		let reply = blocking_send(
			&x,
			&randr::GetScreenSizeRange { window: root }
		)?;

		(
			reply.min_width(),
			reply.min_height(),
			reply.max_width(),
			reply.max_height()
		)
	};

	let res = blocking_send(&x, &randr::GetScreenResources { window: root })?;
	let config_timestamp = res.config_timestamp();

	let mut crtcs: Vec<(
		randr::GetCrtcInfoReply,
		randr::GetPanningReply,
		randr::GetCrtcTransformReply
	)> = Vec::with_capacity(res.crtcs().len());
	for &crtc_id in res.crtcs() {
		let crtc = blocking_send(&x, &randr::GetCrtcInfo { crtc: crtc_id, config_timestamp })?;
		let pan  = blocking_send(&x, &randr::GetPanning { crtc: crtc_id })?;
		let cxf  = blocking_send(&x, &randr::GetCrtcTransform { crtc: crtc_id })?;
		crtcs.push((crtc, pan, cxf));
	}

	let mut outputs: Vec<(
		randr::GetOutputInfoReply,
		String,
		randr::Connection,
	)> =
		Vec::with_capacity(res.outputs().len());
	for &output_id in res.outputs() {
		let output = blocking_send(&x, &randr::GetOutputInfo { output: output_id, config_timestamp })?;
		let name = String::from_utf8(output.name().to_vec())?;
		let outc = output.connection();
		outputs.push((output, name, outc));
	}

	println! (
		"Screen {}: minimum {} x {}, current {} x {}, maximum {} x {}",
		screen_num,
		minw, minh,
		screen.width_in_pixels(), screen.height_in_pixels(),
		maxw, maxh,
	);

	for (ref oi, ref name, ref oc) in outputs {
		println!("{} {:?}", name, oc);

	}

	Ok(())
}



fn blocking_send<R, C> (conn: &XConn, req: &R) -> Result<C::Reply>
where
	R: xcb::Request<Cookie = C>,
	C: xcb::CookieWithReplyChecked,
{
	let cookie = conn.send_request(req);
	Ok(conn.wait_for_reply(cookie)?)
}

////////////////////////////////////////////////////////////////////////

/*

    :lisbon: xrandr -q
	Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 16384 x 16384
	eDP connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 309mm x 174mm
	   1920x1080     60.05*+
	   1680x1050     60.05  
	   1280x1024     60.05  
	   1440x900      60.05  
	   1280x800      60.05  
	   1280x720      60.05  
	   1024x768      60.05  
	   800x600       60.05  
	   640x480       60.05  
	HDMI-A-0 disconnected (normal left inverted right x axis y axis)
	DisplayPort-0 disconnected (normal left inverted right x axis y axis)
	DisplayPort-1 disconnected (normal left inverted right x axis y axis)



dock up && for i (18 19 20); setxkbmap -option ctrl:nocaps -option compose:prsc us -device $i

case "$1" in
	up)
		l=${2-4};
		c=${3-3};
		r=${4-2};
		xrandr \
			--output eDP                                      --rotate normal --auto \
			--output DisplayPort-$l --right-of eDP            --rotate left   --auto \
			--output DisplayPort-$c --right-of DisplayPort-$l --rotate left   --auto \
			--output DisplayPort-$r --right-of DisplayPort-$c --rotate normal --auto
		;;

	down)
		seq 0 7 \
		| sed 's@^@DisplayPort-@' \
		| xargs -I % echo --output % --off \
		| xargs -t xrandr
		;;

	*)
		usage
		;;
esac



*/
