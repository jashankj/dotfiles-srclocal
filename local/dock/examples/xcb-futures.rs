/*-
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 * Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

//!
//! Experiments in binding futures with xcb.
//!

use std::{
	future::Future,
	pin::Pin,
	sync::{Arc, Mutex},
	task::{Context, Poll, Waker},
	thread,
	time::Duration, marker::PhantomData,
};

use xcb::x;

use color_eyre::eyre::{Result, eyre};

////////////////////////////////////////////////////////////////////////

type XConn = Arc<xcb::Connection>;
struct XConnection {
	conn: Arc<xcb::Connection>,
}

struct XReq<C: xcb::CookieWithReplyChecked, R: xcb::Reply> {
	conn: XConn,
	cookie: C,
	reply: Option<R>
}

impl<C, R> Future for XReq<C, R>
where C: xcb::CookieWithReplyChecked,
	  R: xcb::Reply
{
	type Output = R;
	fn poll (self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output>
	{
		if self.reply.is_some() {
			mem.
		}
		if let Some(x) = self.reply {
			Poll::Ready(x)
		} else {
			Poll::Pending
		}
	}
}

fn send_request<R, C> (conn: XConn, req: &R) -> XReq<C, C::Reply>
where
	R: xcb::Request<Cookie = C>,
	C: xcb::CookieWithReplyChecked,
{
	let cookie = conn.send_request(req);
	XReq { conn, cookie, reply: None }
}

////////////////////////////////////////////////////////////////////////

fn main() -> Result<()>
{
	color_eyre::install()?;

	let (x, screen_num) = xcb::Connection::connect(None)?;
	let x: XConn = Arc::new(x);

	let setup  = x.get_setup();
	let screen = setup.roots().nth(screen_num as usize)
		.ok_or_else(|| eyre!("no {}'th screen?", screen_num))?;
	let root   = screen.root();

	let reply = send_request(
		x.clone(),
		&xcb::randr::GetScreenSizeRange { window: root }
	);

//	XReq { x.clone() };

	Ok(())
}



// fn blocking_send<R, C> (conn: XConn, req: &R) -> Result<C::Reply>
// where
// 	R: xcb::Request<Cookie = C>,
// 	C: xcb::CookieWithReplyChecked,
// {
// 	let cookie = conn.send_request(req);
// 	Ok(conn.wait_for_reply(cookie)?)
// }

////////////////////////////////////////////////////////////////////////
