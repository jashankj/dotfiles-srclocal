/*-
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 * Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
 *
 * Portions derived from xorg/apps/xinput, which is
 *   Copyright 1996-1997 Frederic Lepied, France.
 *   Copyright 2007 Peter Hutterer
 *   Copyright 2009, 2011 Red Hat, Inc.
 *   SPDX-License-Identifier: BSD-0-Clause?
 */

#![allow(unused_variables)]
#![allow(unused_imports)]

use xcb::{
	x, Connection as XConn,
	randr, xinput, xkb
};
use color_eyre::eyre::{Result, eyre};

////////////////////////////////////////////////////////////////////////

fn main () -> Result<()>
{
	color_eyre::install()?;

	let (x, screen_num) = xcb::Connection::connect_with_extensions(
		None,
		&[
			xcb::Extension::Input,
			xcb::Extension::Xkb,
		],
		&[]
	)?;
	let setup  = x.get_setup();
	let screen = setup.roots().nth(screen_num as usize)
		.ok_or_else(|| eyre!("no {}'th screen?", screen_num))?;
	let root   = screen.root();

	let info =
		blocking_send(&x, &xinput::XiQueryDevice { device: xinput::Device::All })?;
	for dev in info.infos() {
		println!("{}", describe(dev));
	}

	Ok(())
}

fn describe (dev: &xinput::XiDeviceInfo) -> String
{
	use xcb::xinput::DeviceType::*;
	let (prefix, tree, role_class) = match dev.r#type() {
		MasterPointer  => ("⎡", None,      Some(("master", "pointer"))),
		SlavePointer   => ("⎜", Some("↳"), Some(("slave",  "pointer"))),
		MasterKeyboard => ("⎣", None,      Some(("master", "keyboard"))),
		SlaveKeyboard  => (" ", Some("↳"), Some(("slave",  "keyboard"))),
		FloatingSlave  => (" ", None,      None),
	};

	format!(
		"{:-1}{} {:-40}\tid={}\t[{}]",
		prefix,
		if let Some(x) = tree { format!("{:>4}", x) } else { String::new() },
		dev.name().to_utf8(),
		dev.device().id(),
		if let Some((role, class)) = role_class {
			format!("{:6} {:8} ({})", role, class, dev.attachment().id())
		} else {
			"floating slave".to_string()
		}
	)
}

fn blocking_send<R, C> (conn: &XConn, req: &R) -> Result<C::Reply>
where
	R: xcb::Request<Cookie = C>,
	C: xcb::CookieWithReplyChecked,
{
	let cookie = conn.send_request(req);
	Ok(conn.wait_for_reply(cookie)?)
}

////////////////////////////////////////////////////////////////////////

/*
    :lisbon: xinput list --long
    ⎡ Virtual core pointer                          id=2    [master pointer  (3)]
    ⎜   ↳ Virtual core XTEST pointer                id=4    [slave  pointer  (2)]
    ⎜   ↳ ETPS/2 Elantech Touchpad                  id=12   [slave  pointer  (2)]
    ⎜   ↳ Logitech Wireless Mouse                   id=14   [slave  pointer  (2)]
    ⎜   ↳ Logitech Wireless Keyboard PID:4023       id=15   [slave  pointer  (2)]
    ⎣ Virtual core keyboard                         id=3    [master keyboard (2)]
        ↳ Virtual core XTEST keyboard               id=5    [slave  keyboard (3)]
        ↳ Power Button                              id=6    [slave  keyboard (3)]
        ↳ Video Bus                                 id=7    [slave  keyboard (3)]
        ↳ Power Button                              id=8    [slave  keyboard (3)]
        ↳ Sleep Button                              id=9    [slave  keyboard (3)]
        ↳ Integrated Camera: Integrated C           id=10   [slave  keyboard (3)]
        ↳ AT Translated Set 2 keyboard              id=11   [slave  keyboard (3)]
        ↳ ThinkPad Extra Buttons                    id=13   [slave  keyboard (3)]
        ↳ Logitech Wireless Keyboard PID:4023       id=16   [slave  keyboard (3)]

    :lisbon: xinput list-props 16
    Device 'Logitech Wireless Keyboard PID:4023':
            Device Enabled (163):   1
            Coordinate Transformation Matrix (165): 1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 1.000000
            libinput Send Events Modes Available (283):     1, 0
            libinput Send Events Mode Enabled (284):        0, 0
            libinput Send Events Mode Enabled Default (285):        0, 0
            Device Node (286):      "/dev/input/event15"
            Device Product ID (287):        1133, 16419


*/
