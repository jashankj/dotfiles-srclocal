/*-
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 * Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
 *
 * Portions derived from xorg/apps/xrandr, which is
 *   Copyright © 2001 Keith Packard, member of The XFree86 Project, Inc.
 *   Copyright © 2002 Hewlett Packard Company, Inc.
 *   Copyright © 2006 Intel Corporation
 *   SPDX-License-Identifier: X11
 */

//!
//! This is a translation of the default `xrandr -q` flow.
//!
//! (The `xrandr(1)` tool is a higgledy-piggledy mess!)
//!

use xcb::{
	x, Connection as XConn,
	randr, xinput, xkb, Xid
};
use color_eyre::eyre::{Result, eyre};

///
/// RANDR has concepts of:
///
/// * screens, which are just X screens;
///
/// * CRTCs, or "CRT controllers", define video timings and a viewport
///   within the larger screen (independent of the overall size of the
///   screen), with zero or more associated video outputs.
///
/// * outputs, which show whatever a CRTC pushes onto them;
///
/// * providers, which represent GPUs or similar devices that provide
///   services to the X server, which have both a set of abilities and
///   of possible roles;
///
/// * monitors
///

struct Screen<'a> {
	x_conn:   &'a XConn,
	x_screen: &'a x::Screen,
	min_size: (u16, u16),
	cur_size: (u16, u16),
	max_size: (u16, u16),
	config_time: x::Timestamp,
}

impl<'a> Screen<'a>
{
	fn from_xcb (x: &'a XConn, x_screen: &'a x::Screen) -> Result<Self>
	{
		let root = x_screen.root();
		let size = blocking_send(x, &randr::GetScreenSizeRange { window: root })?;
		let time = blocking_send(x, &randr::GetScreenResources { window: root })?
			.config_timestamp();

		Ok(Screen {
			x_conn: x,
			x_screen,
			min_size: (size.min_width(), size.min_height()),
			cur_size: (
				x_screen.width_in_pixels(),
				x_screen.height_in_pixels()
			),
			max_size: (size.max_width(), size.max_height()),
			config_time: time,
		})
	}

	fn get_root (&self) -> x::Window
	{
		self.x_screen.root()
	}

	fn get_screen_resources (&self) -> Result<randr::GetScreenResourcesReply>
	{
		blocking_send(
			self.x_conn,
			&randr::GetScreenResources { window: self.get_root() }
		)
	}

	fn get_primary_output_xid (&self) -> Result<u32>
	{
		Ok(
			blocking_send(
				self.x_conn,
				&randr::GetOutputPrimary { window: self.get_root() }
			)?
				.output()
				.resource_id()
		)
	}


	fn describe (&self) -> String
	{
		format! (
			"minimum {} x {}, current {} x {}, maximum {} x {}",
			self.min_size.0, self.min_size.1,
			self.cur_size.0, self.cur_size.1,
			self.max_size.0, self.max_size.1,
		)
	}
}

struct Crtc;
struct Output;
struct Provider;
struct Monitor;



////////////////////////////////////////////////////////////////////////

fn main() -> Result<()>
{
	color_eyre::install()?;

	let (x, screen_num) = xcb::Connection::connect_with_extensions(
		None,
		&[xcb::Extension::RandR],
		&[]
	)?;
	let setup  = x.get_setup();
	let screen = setup.roots().nth(screen_num as usize)
		.ok_or_else(|| eyre!("no {}'th screen?", screen_num))?;
	let root   = screen.root();

	let screen = Screen::from_xcb(&x, &screen)?;

	// get_screen:
	let res = blocking_send(&x, &randr::GetScreenResources { window: root })?;
	let config_timestamp = res.config_timestamp();

	let mut crtcs: Vec<(
		randr::GetCrtcInfoReply,
		randr::GetPanningReply,
		randr::GetCrtcTransformReply
	)> = Vec::with_capacity(res.crtcs().len());
	for &crtc_id in res.crtcs() {
		let crtc = blocking_send(&x, &randr::GetCrtcInfo { crtc: crtc_id, config_timestamp })?;
		let pan  = blocking_send(&x, &randr::GetPanning { crtc: crtc_id })?;
		let cxf  = blocking_send(&x, &randr::GetCrtcTransform { crtc: crtc_id })?;
		crtcs.push((crtc, pan, cxf));
	}

	let mut outputs: Vec<(
		randr::GetOutputInfoReply,
		String,
		randr::Connection,
	)> =
		Vec::with_capacity(res.outputs().len());
	for &output_id in res.outputs() {
		let output = blocking_send(&x, &randr::GetOutputInfo { output: output_id, config_timestamp })?;
		let name = String::from_utf8(output.name().to_vec())?;
		let outc = output.connection();
		outputs.push((output, name, outc));
	}

    println! ("Screen {}: {}", screen_num, screen.describe());


	for (ref oi, ref name, ref oc) in outputs {
		println!("{} {:?}", name, oc);
	}

	Ok(())
}



fn blocking_send<R, C> (conn: &XConn, req: &R) -> Result<C::Reply>
where
	R: xcb::Request<Cookie = C>,
	C: xcb::CookieWithReplyChecked,
{
	let cookie = conn.send_request(req);
	Ok(conn.wait_for_reply(cookie)?)
}

////////////////////////////////////////////////////////////////////////

/*

    :lisbon: xrandr -q
	Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 16384 x 16384
	eDP connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 309mm x 174mm
	   1920x1080     60.05*+
	   1680x1050     60.05  
	   1280x1024     60.05  
	   1440x900      60.05  
	   1280x800      60.05  
	   1280x720      60.05  
	   1024x768      60.05  
	   800x600       60.05  
	   640x480       60.05  
	HDMI-A-0 disconnected (normal left inverted right x axis y axis)
	DisplayPort-0 disconnected (normal left inverted right x axis y axis)
	DisplayPort-1 disconnected (normal left inverted right x axis y axis)





*/
