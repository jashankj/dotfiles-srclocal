/*-
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 * Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
 *
 * Portions derived from xorg/apps/setxkbmap, which is
 *   Copyright 1996 Silicon Graphics Computer Systems, Inc.
 *   SPDX-License-Identifier: X11
 */

#![allow(unused_variables)]
#![allow(unused_imports)]

use xcb::{
	x, Connection as XConn,
	xinput, xkb
};
use color_eyre::eyre::{Result, WrapErr, eyre};

////////////////////////////////////////////////////////////////////////

fn main() -> Result<()>
{
	color_eyre::install()?;

	let (x, screen_num) = xcb::Connection::connect_with_extensions(
		None,
		&[
			xcb::Extension::Input,
			xcb::Extension::Xkb,
		],
		&[]
	)?;
	let setup  = x.get_setup();
	let screen = setup.roots().nth(screen_num as usize)
		.ok_or_else(|| eyre!("no {}'th screen?", screen_num))?;
	let root   = screen.root();

	blocking_send(&x, &xkb::UseExtension { wanted_major: 1, wanted_minor: 0 })?;

	/*
	XkbDescPtr
	XkbGetKeyboardByName (
		Display *dpy,
		unsigned int device_spec,
		XkbComponentNamesPtr names,
		unsigned int want,
		unsigned int need,
		Bool load);
	/// ALL EVIL
	 */

	let xkb = blocking_send(&x, &xkb::GetKbdByName {
		device_spec: 256, // xkb::Id::UseCoreKbd as xkb::DeviceSpec,
		need: xkb::GbnDetail::all().difference(xkb::GbnDetail::GEOMETRY),
		want: xkb::GbnDetail::all(),
		load: true
	})?;

	println!("{:?}", xkb);

	Ok(())
}



fn blocking_send<R, C> (conn: &XConn, req: &R) -> Result<C::Reply>
where
	R: xcb::Request<Cookie = C>,
	C: xcb::CookieWithReplyChecked,
{
	let cookie = conn.send_request(req);
	Ok(conn.wait_for_reply(cookie)?)
}

////////////////////////////////////////////////////////////////////////
