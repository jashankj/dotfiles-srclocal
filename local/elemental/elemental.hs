-- {-# OPTIONS_GHC -package containers #-}
-- {-# OPTIONS_GHC -package hashable #-}
-- {-# OPTIONS_GHC -package unordered-containers #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}

import           Control.Arrow
import qualified Data.HashMap.Strict as HashMap
import           Data.HashMap.Strict (HashMap)
import           Data.Hashable
import           Data.Ratio
import           GHC.Generics (Generic)

import qualified Radium.Element as Element

data El = B | C | Ca | Cl | Co | Cr | Cu | F | Fe | H | Hg | I | K | Mg | Mn | Mo | N | Na | O | P | S | Se | Si | V | Zn
deriving instance Eq El
deriving instance Show El
deriving instance Generic El
instance Hashable El

elementOf :: El -> Element.Element
elementOf = Element.elementBySymbol . show

-- data Of = Of Int El
-- deriving instance Eq Of
-- deriving instance Show Of

-- nahco3 = [1 `Of` Na, 1 `Of` H, 1 `Of` C, 3 `Of` O]

-- of_sum_0 :: Of -> (El, Int)
-- of_sum_0 (Of n x) = (x, n)

-- of_sum_1 :: [Of] -> HashMap El Int
-- of_sum_1  = (\(Of n x) -> HashMap.insertWith (+) x n) `foldr` HashMap.empty

-- of_sum :: [[Of]] -> HashMap El Int
-- of_sum = foldr1 (Map.unionWith (+)) . map (of_sum_1)

bulk_smiles =
  [ "[Na+].OC([O-])=O"                          -- sodium bicarbonate
  , "[Na+].[O-][N+]([O-])=O"                    -- sodium nitrate
  , "[K+].[K+].[O-]S([O-])(=O)=O"               -- potassium sulfate
  , "[Na+].[Cl-]"                               -- sodium chloride
  , "[O-]P([O-])([O-])=O.[NH4+].[NH4+].[NH4+]"  -- ammonium phosphate
  , "O.[Ca+2].O.O.O.[O-][N+]([O-])" ++          -- calcium nitrate tetrahydrate
    "=O.[O-][N+]([O-])=O"
  , "[Mg+2].[O-]S([O-])(=O)=O.O.O.O.O.O.O.O"    -- magnesium sulfate heptahydrate
  , "C(CN(CC(=O)[O-])CC(=O)[O-])" ++            -- iron chelate
    "N(CC(=O)O)CC(=O)[O-].[Fe+3]"
  ]

trace_smiles = []
  -- Mn S O₄ (H₂ O)
  -- Hg B O₃
  -- Cu S O₄ (H₂ O)₅
  -- Si O₂
  -- Zn S O₄ (H₂ O)
  -- Na I
  -- Na₂ Mo O₄ (H₂ O)₂
  -- Na₂ Se O₃ (H₂ O)₅
  -- Co Cl₂ (H₂ O)₄
  -- Cr Cl₃ (H₂ O)₆
  -- N H₄ V O₄
  -- Ca F₂


bulk_targets =
  map (bulk_apportion) bulk_portions
  where
    bulk_apportion = second (% bulk_total)
    bulk_total = foldr1 (+) . map snd $ bulk_portions
    bulk_portions =
      [ (Na, 1000 {- mg/L -})
      , (K,   700 {- mg/L -})
      , (Cl,  600 {- mg/L -})
      , (N,   400 {- mg/L -})
      , (S,   200 {- mg/L -})
      , (P,   100 {- mg/L -})
      , (Mg,   30 {- mg/L -})
      , (Ca,   20 {- mg/L -})
      , (Fe,    2 {- mg/L -})
      ]


trace_target =
  [ (Mn,  800 {- μg/L -})
  , (B,   450 {- μg/L -})
  , (Cu,  200 {- μg/L -})
  , (Si,  100 {- μg/L -})
  , (Zn,   50 {- μg/L -})
  , (I,    30 {- μg/L -})
  , (Mo,   10 {- μg/L -})
  , (Se,    5 {- μg/L -})
  , (Co,    5 {- μg/L -})
  , (Cr,    5 {- μg/L -})
  , (V,     2 {- μg/L -})
  ]


main = undefined
