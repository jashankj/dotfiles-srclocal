# -*- makefile-bsdmake -*-

#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
#

SUBDIR	= \
	bsd \
	local

.include <bsd.subdir.mk>
